tool
extends Panel

class_name User_Panel

#onready var id_label:Label = $GridContainer/ID_Label
onready var id_display:Label = $ID_Display

#onready var user_label:Label = $GridContainer/username_Label
onready var username_display:Label = $Username_Display



func _ready():
	# make sure the panel scales correctly
	rect_min_size = Vector2(0, id_display.rect_size.y + username_display.rect_size.y + $Seperator.rect_size.y)

func init(user_id:String, user_name:String) -> void:
	id_display.text = user_id
	
	username_display.text = user_name




func get_user_id() -> String:
	return id_display.text

func set_username(new_username:String) -> void:
	username_display.text = new_username
func get_username() -> String:
	return username_display.text
