extends Panel

class_name Network_Menu

const user_panel:PackedScene = preload("res://interface/network_menu/user_panel/User_Panel.tscn")

onready var control_button:Button = $Control_Button
onready var control_label:Label = $Control_Button/Label

onready var swap_button:Button = $Swap_Button

onready var user_list:VBoxContainer = $Connections_Panel/ScrollContainer/VBoxContainer






func _ready():
	
	# get the initial status
	update_buttons()
	
	# listen to changes in the Network
	connect_to_network_signals()

func connect_to_network_signals() -> void:
	var error:int
	
	# both client and server
	error = Network.connect("user_connected_to_server",self, "user_connected" )
	if error != OK:
		print("ERROR: user_connected_to_server error code: " + str(error))
	error = Network.connect("user_changed_name",self, "user_changed_name" )
	if error != OK:
		print("ERROR: user_changed_name error code: " + str(error))
	error = Network.connect("user_disconnected_from_server",self, "user_disconnected" )
	if error != OK:
		print("ERROR: user_disconnected_from_server error code: " + str(error))
	
	# server only
	error = Network.connect("server_started",self, "server_started" )
	if error != OK:
		print("ERROR: server_started error code: " + str(error))
	error = Network.connect("server_stopped",self, "server_stopped" )
	if error != OK:
		print("ERROR: server_stopped error code: " + str(error))
	
	# client only
	error = Network.connect("connected_to_server",self, "connected_to_server" )
	if error != OK:
		print("ERROR: connected_to_server error code: " + str(error))
	error = Network.connect("disconnected_from_server",self, "disconnected_from_server" )
	if error != OK:
		print("ERROR: disconnected_from_server error code: " + str(error))
	


# update what the buttons should display, based on
# the Network state
func update_buttons() -> void:
	
	match Network.get_network_status():
		Network.SERVER_RUNNING:
			control_label.text = "Stop Server"
			swap_button.text = "Swap to client mode"
			
		Network.SERVER_CLOSED:
			control_label.text = "Start Server"
			swap_button.text = "Swap to client mode"
			
		Network.CLIENT_CONNECTED_TO_A_SERVER:
			control_label.text = "Disconnect from the Server"
			swap_button.text = "Swap to server mode"
			
		Network.CLIENT_NOT_CONNECTED_TO_A_SERVER:
			control_label.text = "Connect to the Server"
			swap_button.text = "Swap to server mode"
		
		Network.CLIENT_SEARCHING_FOR_A_SERVER:
			control_label.text = "Stop looking for a server"
			swap_button.text = "Swap to server mode"








##########
# Server #
##########

# client and server

func user_connected(user:Dictionary) -> void:
	var new_user_panel:User_Panel = user_panel.instance()
	user_list.add_child(new_user_panel)
	
	new_user_panel.init(user.device_id, user.name)
	new_user_panel.name = str(user.device_id)

func user_changed_name(user:Dictionary) -> void:
	for child in user_list.get_children():
		if child is User_Panel:
			if child.get_user_id() == user.device_id:
				child.set_username(user.name)

func user_disconnected(user:Dictionary) -> void:
	for child in user_list.get_children():
		if child is User_Panel:
			if child.name == user.device_id:
				child.queue_free()
				return



# server

func server_started() -> void:
	pass

func server_stopped() -> void:
	
	# clear the list of people connected to the server
	for child in user_list.get_children():
		child.queue_free()


# client

func connected_to_server() -> void:
	update_buttons()

func disconnected_from_server() -> void:
	update_buttons()
	
	# clear the list of people connected to the server
	for child in user_list.get_children():
		child.queue_free()





#######
# GUI #
#######

# make sure the buttons show the correct text
# whenever the menu is opened
func show() -> void:
	update_buttons()
	# call the actual "show" method
	.show()

func _on_Control_Button_pressed():
	
	match Network.get_network_status():
		Network.SERVER_RUNNING:
			Network.stop_server()
			
		Network.SERVER_CLOSED:
			Network.start_server()
			
		Network.CLIENT_CONNECTED_TO_A_SERVER:
			Network.disconnect_from_server()
			
		Network.CLIENT_NOT_CONNECTED_TO_A_SERVER:
			Network.start_server_search()
		
		Network.CLIENT_SEARCHING_FOR_A_SERVER:
			Network.stop_server_search()
	
	update_buttons()


func _on_Swap_Button_pressed():
	if Network.get_operating_mode() == Network.CLIENT_MODE:
		# now in server mode
		Network.set_operating_mode(Network.SERVER_MODE)
	else:
		# now in client mode
		Network.set_operating_mode(Network.CLIENT_MODE)
	
	update_buttons()


func _on_Exit_Button_pressed():
	self.hide()
