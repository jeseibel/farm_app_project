extends Node2D

signal field_pressed(field)

var width:int = 0
var height:int = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	width = $Sprite.texture.get_size().x
	height = $Sprite.texture.get_size().y
	
	$Background.rect_size = Vector2(width,height)
	$Background.rect_position = Vector2(-width/2,-height/2)
	
	
	# connect each field to the map
	var fields:Array = $Fields.get_children()
	
	for item in fields:
		if item is Field:
			
			var error:int = item.connect("field_pressed", self, "_on_field_pressed")
			if error != OK:
				print("ERROR: " + str(item.field_name) + " connection error code: " + str(error))
	
	
	
	# connect each farm to the map
	var farms:Array = $Farms.get_children()
	
	for item in farms:
		# farms are represented as field objects
		if item is Field:
			
			var error:int = item.connect("field_pressed", self, "_on_field_pressed")
			if error != OK:
				print("ERROR: " + str(item.field_name) + " connection error code: " + str(error))
	

func _on_field_pressed(field:Field):
	emit_signal("field_pressed",field)

