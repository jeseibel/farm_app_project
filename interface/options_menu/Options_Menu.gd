class_name Options_Menu

extends Panel

onready var time_option:Resizable_Button = $ScrollContainer/VBoxContainer/Time_Option
onready var map_view_option:Resizable_Button = $ScrollContainer/VBoxContainer/Map_View_Option
onready var task_scroll_option:Resizable_Button = $ScrollContainer/VBoxContainer/Task_Scroll_Option
onready var ip_editor:Line_Editor = $ScrollContainer/VBoxContainer/IP_Editor
onready var username_editor:Line_Editor = $ScrollContainer/VBoxContainer/Username_Editor

# don't allow the buttons to send signals until
# after they have been set up
var loaded:bool = false

func _ready():
	
	time_option.set_text("Show time as relative to the current time")
	
	map_view_option.set_text("Default to a map view of fields instead of a satellite view")
	
	task_scroll_option.set_text("Enable task creator auto scroll")
	
	username_editor.set_title("username:")
	
	ip_editor.set_title("IP (no port):")
	
	# get the current options from the config_handler
	
	var info:Config_Info = Config_Handler.get_info()
	
	time_option.pressed = info.display_time_as_relative
	
	map_view_option.pressed = info.show_field_map_view_by_default
	
	task_scroll_option.pressed = info.enable_auto_scrolling
	
	username_editor.set_editor_text(info.username)
	
	ip_editor.set_editor_text(info.server_ip)
	
	loaded = true





func _on_Time_Option_toggled(pressed:bool):
	if loaded:
		Config_Handler.change_config_option("display_time_as_relative", pressed)

func _on_Map_View_Option_toggled(pressed:bool):
	if loaded:
		Config_Handler.change_config_option("show_field_map_view_by_default", pressed)

func _on_Task_Scroll_Option_toggled(pressed:bool):
	if loaded:
		Config_Handler.change_config_option("enable_auto_scrolling", pressed)







func _on_Exit_Button_pressed():
	self.hide()




func _on_IP_Editor_text_changed(new_text:String):
	if loaded:
		Config_Handler.change_config_option("server_ip", new_text)


func _on_Username_Editor_text_changed(new_text:String):
	if loaded:
		Config_Handler.change_config_option("username", new_text)
