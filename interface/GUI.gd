extends CanvasLayer

signal show_field_on_map(field)



const task_creator_scene:PackedScene = preload("res://interface/task_creator/Task_Creator.tscn")

onready var menu_background:Panel = $Top/Hamburger_Control/Menu_Background
onready var menu_button:Button = $Top/Hamburger_Control/Menu_Button
onready var server_button:Button = $Top/Hamburger_Control/Menu_Background/Server_Button
#onready var network_button:Button = $Top/Hamburger_Control/Network_Indicator/Network_Button

onready var ham_control:Control = $Top/Hamburger_Control

# these are used when showing and hiding the menu
onready var ham_control_left_closed_anchor:float = ham_control.anchor_left
onready var ham_control_right_closed_anchor:float = ham_control.anchor_right


onready var notify_button:Button = $Top/Notification_Button
onready var notif_menu:Notification_Menu = $Top/Notification_Menu

onready var blocker:ColorRect = $Top/Bottom/Blocker
onready var connection_blocker:ColorRect = $Top/Bottom/No_Connection_Blocker

onready var options_menu:Options_Menu = $Top/Options_Menu
onready var help_window:Help_Menu = $Top/Help_Menu
onready var network_menu:Network_Menu = $Top/Network_Menu



var task_creator:Task_Creator

# is the hamburger menu open?
var ham_menu_open:bool = false




################
# constructors #
################

func _ready():
	var os:String = OS.get_name()
	
	# listen to the network
	var error:int = Network.connect("task_created", self, "on_task_created")
	if error != OK:
		print("ERROR: GUI task_created connection error code: " + str(error))
	
	error = Network.connect("task_finished", self, "on_task_finished")
	if error != OK:
		print("ERROR: GUI task_finished connection error code: " + str(error))
	
	# client connection
	error = Network.connect("connected_to_server", self, "hide_connection_blocker")
	if error != OK:
		print("ERROR: GUI connected_to_server connection error code: " + str(error))
	# server status
	error = Network.connect("server_started", self, "hide_connection_blocker")
	if error != OK:
		print("ERROR: GUI server_started connection error code: " + str(error))
	
	
	# if this is a server that automatically starts
	# this menu wouldn't have been created untill after the server
	# would have sent out the "task_created" signals
	# ask the server if it has any unfinished tasks so we can show them here
	var unfinished_tasks:Array = Network.get_unfinished_tasks()
	notify_button.text = str(unfinished_tasks.size())
	
	
	# don't let mobile devices run a server
	if os == "Android" || os == "iOS" || os == "HTML5":
		server_button.hide()
	
	# if the server is currently running, hide the network blocker
	if Config_Handler.get_info().auto_run_server:
		hide_connection_blocker()


func set_option_menu_defaults(info:Config_Info) -> void:
	options_menu.init(info)







#############
# functions #
#############


func open_hamburger_menu():
	ham_control.anchor_left = 0
	ham_control.anchor_right = 0.7
	
	notify_button.hide()
	blocker.show()
	
	ham_menu_open = true

func close_hamburger_menu():
	ham_control.anchor_left = ham_control_left_closed_anchor
	ham_control.anchor_right = ham_control_right_closed_anchor
	
	notify_button.show()
	blocker.hide()
	
	ham_menu_open = false


func show_connection_blocker() -> void:
	connection_blocker.show()

func hide_connection_blocker() -> void:
	connection_blocker.hide()


func open_notification_menu():
	notif_menu.show()
	
	notify_button.hide()
	blocker.show()
	
	ham_menu_open = true

func close_notification_menu():
	notif_menu.hide()
	
	notify_button.show()
	blocker.hide()
	
	ham_menu_open = false



# the options are given in a 2D array with the first element
# in each array being the title of that option list.
func open_task_creator(field:Field):
	var new_creator:Task_Creator = task_creator_scene.instance()
	task_creator = new_creator
	add_child(task_creator)
	
	task_creator.init(field)
	
	var error:int = task_creator.connect("closed", self, "_on_Task_Creator_closed")
	if error != OK:
		print("ERROR: task_creator closed connection error code: " + str(error))
	
	task_creator.show()











################
# misc signals #
################


func _on_Notification_Menu_show_field_on_map(field:Field):
	close_notification_menu()
	
	emit_signal("show_field_on_map", field)


func _on_task_done(_task:Task):
	notify_button.text = str(int(notify_button.text) - 1)

func on_task_created(_user:Dictionary, _task:Task) -> void:
	notify_button.text = str(int(notify_button.text) + 1)

func on_task_finished(_user:Dictionary, _task:Task) -> void:
	notify_button.text = str(int(notify_button.text) - 1)






###############
# gui signals #
###############


func _on_Notification_Button_pressed():
	open_notification_menu()

func _on_Notification_Menu_exit_button_pressed():
	close_notification_menu()


func _on_Control_Button_pressed():
	if ham_menu_open:
		close_hamburger_menu()
	else:
		open_hamburger_menu()


func _on_Options_Button_pressed():
	options_menu.show()


func _on_Help_Button_pressed():
	help_window.show()


func _on_Task_Creator_closed():
	task_creator.queue_free()


func _on_Server_Button_pressed():
	network_menu.show()


func _on_Network_Button_pressed():
	match Network.get_network_status():
		Network.CLIENT_CONNECTED_TO_A_SERVER:
			Network.disconnect_from_server()
		Network.CLIENT_NOT_CONNECTED_TO_A_SERVER:
			Network.start_server_search()
		Network.CLIENT_SEARCHING_FOR_A_SERVER:
			Network.stop_server_search()
