class_name Notification_Menu

extends Panel

signal show_field_on_map(field)
signal exit_button_pressed()

var task_panel:PackedScene = preload("res://interface/notification_menu/task_panel/Task_Panel.tscn")

onready var container:ScrollContainer = $ScrollContainer
onready var list:VBoxContainer = $ScrollContainer/VBoxContainer
onready var no_tasks_label:Label = $No_Tasks_Label

onready var field_button:Button = $Field_Button
onready var field_viewer:Field_Viewer = $Field_Button/Field_Viewer

onready var popup:Confirmation_Popup = $Confirmation_Popup

# these variables are used to remember which task
# is pending completion and which task_panel
# it belongs too
var completed_task:Task
var completed_task_panel:Task_Panel





################
# constructors #
################

func _ready():
	# if this isn't manually set it defaults to ~(12,12)
	# which causes the parking indicator to scale incorrectly
	field_viewer.set_default_panel_size(field_viewer.rect_size)
	
	var error:int = Network.connect("task_created", self, "add_remote_task")
	if error != OK:
		print("Notification_Menu task_created connection error code: " + str(error))
	
	error = Network.connect("task_finished", self, "on_network_task_finished")
	if error != OK:
		print("Notification_Menu task_finished connection error code: " + str(error))
	
	
	# if this is a server that automatically starts
	# this menu wouldn't have been created untill after the server
	# would have sent out the "task_created" signals
	# ask the server if it has any unfinished tasks so we can show them here
	var unfinished_tasks:Array = Network.get_unfinished_tasks()
	for task in unfinished_tasks:
		add_task(task)
	
	
	# debug code
#	var task:Task = Task.new()
#	task.create_dummy_task()
#	add_task(task)
#	add_task(task)
#	add_task(task)
#	add_task(task)
#	add_task(task)








###########
# Signals #
###########

# gui

func _on_Exit_Button_pressed() -> void:
	field_button.visible = false
	emit_signal("exit_button_pressed")

func _on_Field_Button_pressed() -> void:
	field_button.visible = false


# show the popup window to make sure
# the user is done
func on_done_button_pressed(task:Task, new_task_panel:Task_Panel) -> void:
	completed_task = task
	completed_task_panel = new_task_panel
	
	popup.show()

func _on_popup_yes_button_pressed():
	
	# am I connected to a server or a server?
	if get_tree().network_peer != null:
		# am I the server or the clinet?
		if get_tree().network_peer.get_unique_id() == 1:
			# I am the server
			
			Network.task_done(completed_task.create_dictionary())
			
		else:
			# I am a client
			
			# tell the server the task is done
			rpc_id(1, "tell_the_network_a_task_is_done", completed_task.create_dictionary())
	
	# I am not connected to the server
	else:
		
		# store the completed task so the server can be notified when I reconnect
		Network.completed_tasks.append(completed_task)
		
		# hide the task_pane so it can be removed by the server later
		completed_task_panel.hide()
		
		# are they any other tasks?
		if list.get_child_count() == 1:
			# nope, show the 'no tasks text'
			no_tasks_label.visible = true
	
	popup.hide()

remote func tell_the_network_a_task_is_done(task_dict:Dictionary) -> void:
	Network.task_done(task_dict)



func _on_popup_no_button_pressed() -> void:
	completed_task = null
	completed_task_panel = null
	
	popup.hide()





###########
# methods #
###########

# network

remote func add_remote_task(_user:Dictionary, task:Task) -> void:
	add_task(task)

func add_task(task:Task) -> void:
	var new_task_panel:Task_Panel = task_panel.instance()
	list.add_child(new_task_panel)
	
	
	var error:int = new_task_panel.connect("show_enlarged_field",self,"show_enlarged_field")
	if error != 0:
		print("new_task_panel show_enlarged_field connection error code: " + str(error))
	
	error = new_task_panel.connect("show_field_on_map",self,"show_field_on_map")
	if error != 0:
		print("new_task_panel show_field_on_map connection error code: " + str(error))
	
	error = new_task_panel.connect("done_button_pressed",self,"on_done_button_pressed", [ new_task_panel ])
	if error != 0:
		print("new_task_panel done_button_pressed connection error code: " + str(error))
	
	
	new_task_panel.rect_min_size = Vector2(0,container.rect_size.y)
	# if we are a server show who each task is for
	# if not, don't show who each task is for
	new_task_panel.init(task, Network.get_operating_mode() == Network.SERVER_MODE)
	
	no_tasks_label.visible = false

func on_network_task_finished(_sender:Dictionary, task:Task) -> void:
	
	# remove the task_panel
	for panel in list.get_children():
		if panel is Task_Panel:
			if panel.get_task().is_equal(task):
				
				list.remove_child(panel)
				panel.queue_free()
				break
	
	# are they any other tasks?
	if list.get_child_count() == 0:
		# nope, show the 'no tasks text'
		no_tasks_label.visible = true



# gui

func show_enlarged_field(task:Task, map_view:bool) -> void:
	field_button.visible = true
	
	# set the field texture
	if map_view:
		field_viewer.set_field_texture(task.field.map_texture)
	else:
		field_viewer.set_field_texture(task.field.satellite_texture)
	
	# set the paint texture
	var itex:ImageTexture = ImageTexture.new()
	if task.goal.image != null:
		itex.create_from_image(task.goal.image)
		field_viewer.set_field_painter_texture(itex)
	
	# set the parking_indicators location
	field_viewer.set_parking_indicator_location(task.parking_location)
	field_viewer.set_parking_indicator_rotation(task.parking_direction)
	field_viewer.set_parking_indicator_visibility(true)

func show_field_on_map(field:Field) -> void:
	emit_signal("show_field_on_map", field)

