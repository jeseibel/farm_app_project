class_name Task_Panel

extends ColorRect

signal show_enlarged_field(task, show_map_view)
signal show_field_on_map(field)
signal done_button_pressed(task)


onready var container:VBoxContainer = $Background/Scroll_Background/ScrollContainer/VBoxContainer
onready var who_button:Resizable_Button = $Background/Scroll_Background/ScrollContainer/VBoxContainer/Who
onready var when_button:Resizable_Button = $Background/Scroll_Background/ScrollContainer/VBoxContainer/When
onready var vehicle_button:Resizable_Button = $Background/Scroll_Background/ScrollContainer/VBoxContainer/vehicle
onready var attatchment_button:Resizable_Button = $Background/Scroll_Background/ScrollContainer/VBoxContainer/Attatchment
onready var goal_button:Resizable_Button = $Background/Scroll_Background/ScrollContainer/VBoxContainer/Goal
onready var info_button:Resizable_Button = $Background/Scroll_Background/ScrollContainer/VBoxContainer/Info

onready var field_viewer:Field_Viewer = $Background/Field_Background/Field_Viewer

onready var map_satellite_button:Button = $Background/map_satellite_button
onready var view_on_map_button:Button = $Background/view_on_map_button
onready var done_button:Button = $Background/done_button

var show_time_as_relative = Config_Handler.get_info().display_time_as_relative

var task:Task

var parking_location:Vector2

var map_view:bool = true





################
# constructors #
################

func _ready():
	
	var error:int = Config_Handler.connect("option_changed", self, "on_option_changed")
	if error != OK:
		print("ERROR: Config_Handler -> Task_Panel option_changed connection error code: " + str(error))
	
	# debug constructor
#	var new_task:Task = Task.new()
#	new_task.create_dummy_task()
#	init(new_task, true)

func init(new_task:Task, show_who:bool) -> void:
	
	map_view = Config_Handler.get_info().show_field_map_view_by_default
	
	task = new_task
	
	# add the who text
	who_button.set_text(task.who)
	if !show_who:
		who_button.hide()
	
	
	# add the time text
	when_button.set_text(Time_Helper.create_time_string(task.when, show_time_as_relative))
	
	# add the vehicle text
	vehicle_button.set_text(task.vehicle)
	
	# add the attatchment text
	attatchment_button.set_text(task.attatchment)
	
	# add the goal text
	goal_button.set_text(task.goal.name)
	
	# add the info text (if there is any)
	if task.special_instructions.empty():
		info_button.hide()
	else:
		info_button.set_text(task.special_instructions)
	
	# add the field image
	if map_view:
		field_viewer.set_field_texture(task.field.get_map_texture())
		map_satellite_button.icon = load("res://images/gui/map.png")
	else:
		field_viewer.set_field_texture(task.field.get_satellite_texture())
		map_satellite_button.icon = load("res://images/gui/satellite.png")
	
	# add the painter image (if it has one)
	if task.goal.image != null:
		var itex:ImageTexture = ImageTexture.new()
		itex.create_from_image(task.goal.image)
		field_viewer.set_field_painter_texture(itex)
	
	# add the parking location/rotation
	field_viewer.set_parking_indicator_visibility(true)
	field_viewer.scale_parking_indicator(0.75 / Scale_Handler.get_screen_scale())
	field_viewer.set_parking_indicator_location(task.parking_location)
	field_viewer.set_parking_indicator_rotation(task.parking_direction)





###############
# time update #
###############

func _physics_process(_delta:float):
	# update the time
	if task != null && when_button != null:
		when_button.set_text(Time_Helper.create_time_string(task.when, show_time_as_relative))
	update()






###########
# signals #
###########

func _on_Field_Button_pressed():
	emit_signal("show_enlarged_field", task, map_view)


func _on_view_on_map_button_pressed():
	emit_signal("show_field_on_map", task.field)

func _on_map_satellite_button_pressed():
	if map_view:
		field_viewer.set_field_texture(task.field.get_satellite_texture())
		map_satellite_button.icon = load("res://images/gui/satellite.png")
		map_view = false;
	else:
		field_viewer.set_field_texture(task.field.get_map_texture())
		map_satellite_button.icon = load("res://images/gui/map.png")
		map_view = true


func _on_done_button_pressed():
	emit_signal("done_button_pressed", task)





#############
# functions #
#############

func get_task() -> Task:
	return task

func on_option_changed(option_name:String, option):
	if option_name == "show_field_map_view_by_default":
		if option is bool:
			map_view = option as bool
			
			if map_view:
				field_viewer.set_field_texture(task.field.get_map_texture())
				map_satellite_button.icon = load("res://images/gui/map.png")
			else:
				field_viewer.set_field_texture(task.field.get_satellite_texture())
				map_satellite_button.icon = load("res://images/gui/satellite.png")
		else:
			print("ERROR: show_field_map_view_by_default was given a non boolean option")
		
	elif option_name == "display_time_as_relative":
		if option is bool:
			show_time_as_relative = option as bool
		else:
			print("ERROR: show_time_as_relative was given a non boolean option")
