tool
class_name Resizable_Button

extends Button

export(Texture) var checked_texture
export(Texture) var unchecked_texture

export(Texture) var radio_selected_texture
export(Texture) var radio_unselected_texture


onready var label:Label = $Label
onready var texture_rect:TextureRect = $TextureRect

var selected_texture:Texture
var unselected_texture:Texture

# should the button resize every time it changes size?
var auto_fit:bool = true

# This prevents infinite recursion when resizing the
# label
var current_resizing:bool = false

func _ready():
	
	# in case make_radio_button was called before _ready
	if group == null:
		selected_texture = checked_texture
		unselected_texture = unchecked_texture
	else:
		selected_texture = radio_selected_texture
		unselected_texture = radio_unselected_texture
	
	texture_rect.texture = unselected_texture


func set_auto_fit(new_auto_fit:bool) -> void:
	auto_fit = new_auto_fit
func get_auto_fit() -> bool:
	return auto_fit

func set_textures(new_selected_texture:Texture, new_unselected_texture:Texture):
	selected_texture = new_selected_texture
	unselected_texture = new_unselected_texture
	
	if pressed:
		texture_rect.texture = selected_texture
	else:
		texture_rect.texture = unselected_texture


# turn this check button into a radio button with the given group
func make_radio_button(new_button_group:ButtonGroup) -> void:
	selected_texture = radio_selected_texture
	unselected_texture = radio_unselected_texture
	
	if pressed:
		texture_rect.texture = selected_texture
	else:
		texture_rect.texture = unselected_texture
	
	group = new_button_group




func _on_Button_toggled(button_pressed:bool):
	if button_pressed:
		if unselected_texture != null:
			texture_rect.texture = selected_texture
	else:
		if unselected_texture != null:
			texture_rect.texture = unselected_texture



func set_text(new_text:String) -> void:
	if label != null:
		label.text = new_text

func get_text() -> String:
	return label.text



func _on_Label_resized():
	# only resize the button if the _ready
	# method has been called
	if auto_fit && label != null && !current_resizing:
		current_resizing = true
		resize_button()
		current_resizing = false


func resize_button():
	
	# set the minimum height to the label's height
	rect_min_size = Vector2(0,label.rect_size.y)
	# shrink down to fit the label's height
	# and a little bit slimmer (this is to force
	# the label to resize itself aswell)
	rect_size = Vector2(rect_size.x - 1,0)
	
	# set the minimum height to the label's height,
	# the label may now be a different size since
	# it was forced to resize earlier
	rect_min_size = Vector2(0,label.rect_size.y)
	# shrink down to fit the label's height
	rect_size = Vector2(rect_size.x + 1,0)


func _on_Label_minimum_size_changed():
	current_resizing = true
	resize_button()
	current_resizing = false
