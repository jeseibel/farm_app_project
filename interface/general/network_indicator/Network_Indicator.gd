extends TextureRect

class_name Network_Indicator

onready var animated_icon:AnimatedSprite = $AnimatedSprite

export(Texture) var connected_texture:Texture
export(Texture) var disconnected_texture:Texture
export(SpriteFrames) var looking_for_connection_animation:SpriteFrames
export(Texture) var connection_error_texture:Texture

export(Texture) var server_running_texture:Texture
export(Texture) var server_down_texture:Texture



func _ready():
	var error:int
	
	
	# get the server's status
	match Network.get_network_status():
		Network.CLIENT_CONNECTED_TO_A_SERVER:
			texture = connected_texture
		Network.CLIENT_NOT_CONNECTED_TO_A_SERVER:
			texture = disconnected_texture
		Network.SERVER_RUNNING:
			texture = server_running_texture
		Network.SERVER_CLOSED:
			texture = server_down_texture
	
	
	
	# listen to Network changes
	
	
	# client
	
	error = Network.connect("connected_to_server", self, "on_connected_to_server")
	if error != OK:
		print("ERROR: Network_indicator connected_to_server error code: " + str(error))
	
	error = Network.connect("disconnected_from_server", self, "on_disconnected_to_server")
	if error != OK:
		print("ERROR: Network_indicator connected_to_server error code: " + str(error))
	
	error = Network.connect("searching_for_server", self, "on_searching_for_server")
	if error != OK:
		print("ERROR: Network_indicator connected_to_server error code: " + str(error))
	
	# since this generally only happens when searching for a server, don't display it
#	error = Network.connect("server_connection_fail", self, "on_server_connection_fail")
#	if error != OK:
#		print("ERROR: Network_indicator connected_to_server error code: " + str(error))
	
	
	
	# server
	
	error = Network.connect("server_started", self, "on_server_started")
	if error != OK:
		print("ERROR: Network_indicator connected_to_server error code: " + str(error))
	
	error = Network.connect("server_stopped", self, "on_server_stopped")
	if error != OK:
		print("ERROR: Network_indicator connected_to_server error code: " + str(error))



# client

func on_connected_to_server() -> void:
	animated_icon.hide()
	texture = connected_texture

func on_disconnected_to_server() -> void:
	animated_icon.hide()
	texture = disconnected_texture

func on_searching_for_server() -> void:
	texture = null
	
	animated_icon.show()
	if animated_icon.frames != looking_for_connection_animation:
		animated_icon.frames = looking_for_connection_animation
	
	# make sure it is scaled correctly
	var min_size:float = min(rect_size.x , rect_size.y)
	animated_icon.scale = Vector2(min_size, min_size) / animated_icon.frames.get_frame("default", 0).get_size()
	animated_icon.position = rect_size / 2
	
	animated_icon.frame = (1 + animated_icon.frame) % animated_icon.frames.get_frame_count("default")

func on_server_connection_fail() -> void:
	animated_icon.hide()
	texture = connection_error_texture



# server

func on_server_started() -> void:
	animated_icon.hide()
	texture = server_running_texture

func on_server_stopped() -> void:
	animated_icon.hide()
	texture = server_down_texture
