class_name Field_Viewer

extends Control

onready var field_image:TextureRect = $Field_TextureRect
onready var painter_image:TextureRect = $Field_TextureRect/Painter_TextureRect
onready var parking_indicator:Sprite = $Parking_Indicator

# variables related to scaling the inidicator
onready var default_indicator_scale:Vector2 = parking_indicator.scale
# this may need to be set manually if it is inside a control element
onready var default_panel_size:Vector2 = rect_size

# this is a percent relative to the center of the field_panel
var parking_location:Vector2


# this can be used to print extra information to the console if needed
var debug_mode:bool = false
# this variable is used to prevent the console from spamming
# warning messages
var warned:bool = false



func _ready():
	
	# scale this based on the screen's DPI
	default_indicator_scale = default_indicator_scale * Scale_Handler.get_screen_scale()



# this may need to be set if the parking indicator
# doesn't scale correctly
func set_default_panel_size(new_size:Vector2) -> void:
	default_panel_size = new_size


func set_field_texture(new_texture:Texture) -> void:
	field_image.texture = new_texture
func get_field_texture() -> Texture:
	return field_image.texture

func set_field_painter_texture(new_texture:Texture) -> void:
	painter_image.texture = new_texture
func get_field_painter_texture() -> Texture:
	return painter_image.texture 


func set_parking_indicator_location(new_pos:Vector2) -> void:
	parking_location = new_pos
	# update the inidications position
	reposition_indicator()
	# update the parking_location
	parking_location = create_parking_location_percentage()
# this is a percentage relative to the field image.
# with (0,0) being the top right and (1,1) being the bottom left
func get_parking_indicator_location() -> Vector2:
	return parking_location

func set_parking_indicator_pixel_position(new_pos:Vector2) -> void:
	parking_indicator.position = new_pos
	# update the parking_location
	parking_location = create_parking_location_percentage()
	# update the parking_location
	reposition_indicator()
func get_parking_indicator_pixel_position() -> Vector2:
	return parking_indicator.position

func set_parking_indicator_rotation(new_rot:float) -> void:
	parking_indicator.rotation = new_rot
func get_parking_indicator_rotation() -> float:
	return parking_indicator.rotation

func set_parking_indicator_scale(new_scale:Vector2) -> void:
	default_indicator_scale = new_scale
func get_parking_indicator_scale() -> Vector2:
	return parking_indicator.scale
# scale the parking indicator by this amount
func scale_parking_indicator(scale:float) -> void:
	default_indicator_scale = default_indicator_scale * scale


func set_parking_indicator_visibility(new_vis:bool) -> void:
	parking_indicator.visible = new_vis



# determine the location of the parking indicator as
# a percentage relative to the field image.
# with (0,0) being the top right and (1,1) being the bottom left
func create_parking_location_percentage() -> Vector2:
	# this is the shortest side of the image
	# (since when it scales the side lengths change)
	var smallest_side:float = min(rect_size.x, rect_size.y)
	# this is how big the feld image is
	var image_size:Vector2 = Vector2(smallest_side,smallest_side)
	
	# this is where the image is centered about
	var center:Vector2 = rect_size / 2
	# this is how far the image is from the edge of the container
	var dist_of_image_from_container_edge:Vector2 = center - (image_size / 2)
	
	# this is how far into the field (in pixels) the indicator is
	var dist_of_indicator_into_field:Vector2 = parking_indicator.position - dist_of_image_from_container_edge
	
	# percent is the indicator's percent into the image
	# 0 = top/left, 1 = bottom/right
	var percent:Vector2 = dist_of_indicator_into_field / image_size
	
	return percent

# move the indicator relative to the size and scale of the field image
# (NOTE: this assumes the field is a square image, with north being up)
func reposition_indicator() -> void:
	# percent is the indicator's percent into the image
	# 0 = top/left, 1 = bottom/right
	var percent:Vector2 = parking_location
	
	# this is the shortest side of the image
	# (since when it scales the side lengths change)
	var smallest_side:float = min(rect_size.x, rect_size.y)
	# this is how big the feld image is
	var image_size:Vector2 = Vector2(smallest_side,smallest_side)
	
	# this is where the image is centered about
	var center:Vector2 = rect_size / 2
	
	# this is how many pixels into the image the indicator should be
	var dist_of_indicator:Vector2 = image_size * percent
	
	# this is how far the image is from the edge of the container
	var dist_of_image_from_container_edge:Vector2 = center - (image_size / 2)
	
	# this is the final pixel location of the indicator
	var final:Vector2 = dist_of_image_from_container_edge + dist_of_indicator
	
	parking_indicator.position = final
	
	# update the scale of the indicator as well
	scale_indicator()

# rescale the indicator based on how big the Field_Viewer
# started
func scale_indicator() -> void:
	
	# if the default_panel_size has an area of zero
	# this code won't work correctly
	if default_panel_size.x == 0 || default_panel_size.y == 0:
		
		# try to set the default_panel_size, since the first time
		# must have failed
		default_panel_size = rect_size
		
		# the rect_size was a valid size and replaced the default_panel_size
		if default_panel_size.x != 0 && default_panel_size.y != 0:
			# only print this if the warning was printed before
			if warned && debug_mode:
				print("Info: (Field_Viewer) the default_panel_size has been updated to: " , default_panel_size)
			return
		
		# only print the warning once
		if !warned && debug_mode:
			print("Warning: (Field_Viewer) tried to scale the parking_indicator, but the default_panel_size has zero area, and the current rect_size has zero area.")
			# don't print the warning again
			warned = true
		return
	
	# this is the shortest side of the image
	# (since when it scales the side lengths change)
	var smallest_side:float = min(rect_size.x, rect_size.y)
	# this is how big the feld image is
	var image_size:Vector2 = Vector2(smallest_side,smallest_side)
	
	# make sure the default_panel_size is a square
	# (otherwise the indicator may become stretched)
	if default_panel_size.x != default_panel_size.y:
		default_panel_size = Vector2(min(default_panel_size.x,default_panel_size.y),min(default_panel_size.x,default_panel_size.y))
	
	
	# determine the scale change
	var scale:Vector2 = image_size / default_panel_size
	
	parking_indicator.scale = scale * default_indicator_scale



func _on_Field_Panel_resized():
	
	# if the window changes size, make sure the indicatior stays
	# in the same place relative to the field_panel
	if parking_indicator != null:
		reposition_indicator()
