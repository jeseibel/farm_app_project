tool
class_name Confirmation_Popup

extends ColorRect

signal yes_button_pressed()
signal no_button_pressed()

onready var label:Label = $Container/Label
onready var button_panel:Panel = $Container/Button_Panel

onready var yes_button:Button = $Container/Button_Panel/Yes_Button
onready var no_button:Button = $Container/Button_Panel/No_Button

export(String) var label_text = "Are you sure you want to quit?" setget set_text
# if this is false the popup will just hide itself instead
export(bool) var delete_self_on_button_press = false

func _ready():
	set_text(label_text)
	
	# the button_panel by default should not have a min_size
	# if it does it will mess up the container's centering
	
	# scale the panel to fit the buttons
	button_panel.rect_min_size = yes_button.rect_size * 1.75

func set_text(new_text:String) -> void:
	label_text = new_text
	
	if label != null:
		label.text = new_text
		label.rect_size = Vector2(label.rect_size.x, 0)
		
		update()

func get_text() -> String:
	return label.text



func _on_Yes_Button_pressed():
	emit_signal("yes_button_pressed")
	
	close()

func _on_No_Button_pressed():
	emit_signal("no_button_pressed")
	
	close()


func close():
	if delete_self_on_button_press:
		self.queue_free()
	else:
		self.hide()
