tool
extends Panel

class_name Line_Editor

signal text_changed(new_text)

onready var label:Label = $HBoxContainer/Label
onready var line_edit:LineEdit = $HBoxContainer/LineEdit

onready var timer:Timer = $Timer


func _ready():
	# make sure it scales vertically corretly
	rect_min_size.y = theme.default_font.get_string_size("A").y * 2
	
	

func set_title(new_text:String) -> void:
	label.text = new_text
	label.rect_min_size.x = determine_width_of_label()
func determine_width_of_label() -> float:
	var length:float = 0
	
	for ch in get_title():
		# the / 1.5 is because get_char_size doesn't work in this version
		# of godot, it should work in the next version
		length += theme.default_font.get_char_size(ch as int).x / 1.5
	
	return length


func get_title() -> String:
	return label.text

func set_editor_text(new_text:String) -> void:
	line_edit.text = new_text
func get_editor_text() -> String:
	return line_edit.text

func _on_LineEdit_text_entered(new_text:String):
	emit_signal("text_changed", new_text)


func _on_LineEdit_text_changed(_new_text:String):
	# only send the text after a short time of inactivity
	timer.start()


func _on_Timer_timeout():
	emit_signal("text_changed", line_edit.text)
	timer.stop()
