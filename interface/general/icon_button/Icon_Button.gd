class_name Icon_Button

extends Button

func set_text(new_text:String) -> void:
	$Label.text = new_text

func get_text() -> String:
	return $Label.text


func set_icon_texture(new_texture:Texture) -> void:
	$TextureRect.texture = new_texture

func get_icon_texture() -> Texture:
	return $TextureRect.texture


func show_icon_texture() -> void:
	$TextureRect.show()

func hide_icon_texture() -> void:
	$TextureRect.hide()
