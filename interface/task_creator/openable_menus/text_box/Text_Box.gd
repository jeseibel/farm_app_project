class_name Text_Box

extends Openable_Menu


onready var text_editor:TextEdit = $Panel/TextEdit

onready var done_button:Button = $ColorRect/Done_Button



func init(new_title:String) -> void:
	set_title(new_title)




func open() -> void:
	show()
	set_focus()

func get_text() -> String:
	return text_editor.text

func set_text(text:String) -> void:
	text_editor.text = text
func clear_text() -> void:
	text_editor.text = ""
	text_editor.clear_undo_history()

func set_text_from_task(task:Task) -> void:
	
	if task.get_data_aliases()["vehicle"] == get_title():
		set_text(task.vehicle)
	elif task.get_data_aliases()["attatchment"] == get_title():
		set_text(task.attatchment)
	elif task.get_data_aliases()["goal"] == get_title():
		if task.goal != null:
			set_text(task.goal.name)
		else:
			set_text("")
	elif task.get_data_aliases()["special_instructions"] == get_title():
		set_text(task.special_instructions)



func set_focus() -> void:
	text_editor.grab_focus()


# if the user presses enter
# remove the new line character
# and assume they pressed the done button
func _input(event:InputEvent):
	if text_editor.has_focus():
		if event is InputEventKey:
			if event.scancode == KEY_ENTER:
				text_editor.text = text_editor.text.replace("\n","")
				
				._on_Done_Button_pressed()

