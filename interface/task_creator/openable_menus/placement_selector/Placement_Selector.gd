class_name Placement_Selector

extends Openable_Menu

onready var field_viewer:Field_Viewer = $Field_Viewer
onready var done_button:Button = $ColorRect/Done_Button
onready var map_view_button:Button = $ColorRect/Map_View_Button

# textures for the map button
var map_texture:Texture
var satellite_texture:Texture

# textures for the map_stellite_button
var map_view:bool = true
export(Texture) var map_button_texture
export(Texture) var satellite_button_texture



func _ready():
	
	var error:int = Config_Handler.connect("option_changed", self, "on_option_changed")
	if error != OK:
		print("ERROR: Config_Handler -> Task_Panel option_changed connection error code: " + str(error))
	
	
	# set up the buttons
	map_view_button.icon = map_button_texture
	
	# clear the field image
	field_viewer.set_field_texture(null)
	
	# debug constructor
	init("title", load("res://images/fields/Far_North.png"), null, Vector2(0,0), 0, false)


func init(new_title:String, new_map_texture:Texture, new_satellite_texture:Texture, new_parking_location:Vector2, new_parking_rotation:float, show_indicator:bool) -> void:
	
	set_title(new_title)
	
	# set the given textures for later use
	map_texture = new_map_texture
	satellite_texture = new_satellite_texture
	
	# set wether to have the satellite or map view
	map_view = Config_Handler.get_info().show_field_map_view_by_default
	
	# set the correct background texture and button image
	if map_view:
		map_view_button.icon = map_button_texture
		field_viewer.set_field_texture(map_texture)
	else:
		map_view_button.icon = satellite_button_texture
		field_viewer.set_field_texture(satellite_texture)
	
	
	# set the parking indicator variables
	field_viewer.set_parking_indicator_location(new_parking_location)
	
	field_viewer.set_parking_indicator_rotation(new_parking_rotation)
	
	field_viewer.set_parking_indicator_visibility(show_indicator)

func set_painter_image(new_image:Image) -> void:
	if new_image == null:
		# clear the image
		field_viewer.set_field_painter_texture(null)
		return
	
	var itex:ImageTexture = ImageTexture.new()
	itex.create_from_image(new_image)
	field_viewer.set_field_painter_texture(itex)

# this is a percentage relative to the field image.
# with (0,0) being the top right and (1,1) being the bottom left
func get_parking_location() -> Vector2:
	return field_viewer.get_parking_indicator_location()

func get_parking_rotation() -> float:
	return field_viewer.get_parking_indicator_rotation()




func _input(event:InputEvent) -> void:
	
	# prevent opening the menu counting as input
	if !self.visible:
		return
	
	var display_offset:Vector2 = field_viewer.rect_global_position
	
	
	
	# get the size of the field_viewer
	var top:float = field_viewer.get_global_rect().position.y
	var bottom:float = top + field_viewer.rect_size.y
	var left:float = field_viewer.get_global_rect().position.x
	var right:float = left + field_viewer.rect_size.x
	
	if event is InputEventScreenTouch:
		if event.pressed:
			
			# prevent clicking outside the field_viewer
			if (event.position.y < top || event.position.y > bottom ||
			event.position.x > right || event.position.x < left):
				return
			
			field_viewer.set_parking_indicator_visibility(true)
			field_viewer.set_parking_indicator_pixel_position(event.position - display_offset)
			
			
			# reset the rotation, so it is facing up
			field_viewer.set_parking_indicator_rotation(PI/2)
			
			# enable the done button
			done_button.disabled = false
	
	elif event is InputEventScreenDrag:
		
		# prevent clicking outside the field_viewer
		if (event.position.y < top || event.position.y > bottom ||
		event.position.x > right || event.position.x < left):
			return
		
		field_viewer.set_parking_indicator_rotation(field_viewer.get_parking_indicator_pixel_position().angle_to_point(event.position - display_offset))






func _on_Map_View_Button_pressed():
	if map_view:
		map_view_button.icon = satellite_button_texture
		field_viewer.set_field_texture(satellite_texture)
	else:
		map_view_button.icon = map_button_texture
		field_viewer.set_field_texture(map_texture)
	
	map_view = !map_view



func on_option_changed(option_name:String, option):
	if option_name == "show_field_map_view_by_default":
		if option is bool:
			map_view = option as bool
			
			if map_view:
				map_view_button.icon = map_button_texture
				field_viewer.set_field_texture(map_texture)
			else:
				map_view_button.icon = satellite_button_texture
				field_viewer.set_field_texture(satellite_texture)
			
		else:
			print("ERROR: show_field_map_view_by_default was given a non boolean option")

