tool
extends Control

class_name Time_Control

onready var label:Label = $Label
onready var up_button:Button = $Up_Button
onready var down_button:Button = $Down_Button

enum {MORNING = 0, AFTERNOON = 1}

export(int) var button_increment = 1

export(bool) var use_current_time = false
# starting_time is only used if use_current_time is true
export(int, 0, 59) var starting_time = 0

# the default must be set to "Hour" otherwise it
# defaults to null when "Hour" is selected
export(String, "Hour", "Minute", "AM/PM", "Day") var time_type = "Hour"

# if the type is AM/PM 0 = AM, 1 = PM
var time:int
var max_time = 12
var min_time:int = 1
var date:Time = Time.new()
var total_numb_of_day_diff:int = 0

# only use if the number is not -1
export(int) var max_time_override = -1
export(int) var min_time_override = -1

func _ready():
	
	connect_buttons()
	
	set_starting_time()
	
	update_label()

# because this script is used in multiple places
# the buttons must be connected via code instead
# of the editor
func connect_buttons() -> void:
	var error:int = up_button.connect("pressed", self, "_on_Up_Button_pressed")
	if error != 0:
		print("Time_Control up_utton pressed connection error code: " + str(error))
	
	error = down_button.connect("pressed", self, "_on_Down_Button_pressed")
	if error != 0:
		print("Time_Control down_button pressed connection error code: " + str(error))



func set_starting_time() -> void:
	
	var timeDict:Dictionary
	
	if use_current_time:
		timeDict = OS.get_datetime()
	else:
		# the same value is set for both hour and minute
		# since the script will only ever use one of them
		timeDict = {"hour" : starting_time,
					"minute" : starting_time,
					"day" : OS.get_date()["day"]}
	
	date.set_to_current_time()
	
	
	if time_type == "Hour":
		# set the current time
		if timeDict.hour <= 12:
			time = timeDict.hour
		else:
			time = timeDict.hour - 12
		
		# set what the min and max number can be
		min_time = 1
		max_time = 12
	
	elif time_type == "Minute":
		# set the current time
		time = timeDict.minute
		
		# round the time to the nearest button_increment
		# if the increment is greater than 1
		if button_increment > 1:
			var tmp_time = time % button_increment
			
			if tmp_time == 0:
				time = time - tmp_time
			else:
				time = time - tmp_time + button_increment
		
		# set what the min and max number can be
		min_time = 0
		max_time = 60 - button_increment
	
	elif time_type == "AM/PM":
		# set the current time
		if timeDict.hour < 12:
			time = MORNING
		else:
			time = AFTERNOON
		
		# set what the min and max number can be
		min_time = MORNING
		max_time = AFTERNOON
	
	elif time_type == "Day":
		# set the current time
		time = timeDict.day
		
		# set what the min and max number can be
		min_time = 1
		max_time = Time_Helper.month_days[date.month]
	
	# if the time override is set use it instead
	# of the standard min and max time
	if max_time_override != -1:
		max_time = max_time_override
	if min_time_override != -1:
		min_time = min_time_override
	
	
	# make sure that the current time is within the min and max
	if time > max_time:
		time = max_time
	if time < min_time:
		time = min_time


# set the text for the label
func update_label() -> void:
	# with a special case for AM/PM
	if time_type == "AM/PM":
		if time == MORNING:
			label.text = "AM"
		elif time == AFTERNOON:
			label.text = "PM"
	elif time_type == "Day":
		
		# if the date is today, print 'Today'
		if time == OS.get_date()["day"] && date.month == OS.get_date()["month"]:
			label.text = "Today"
			return
		else:
			
			# determine the day of the week
			var day_of_week:int = ((total_numb_of_day_diff % 7) + OS.get_datetime()["weekday"]) % 7
			# if the date was negative, wrap it around
			if day_of_week < 0:
				day_of_week += 7
			
			var day_name:String
			match day_of_week:
				0:
					day_name = "Sun"
				1:
					day_name = "Mon"
				2:
					day_name = "Tue"
				3:
					day_name = "Wed"
				4:
					day_name = "Thu"
				5:
					day_name = "Fri"
				6:
					day_name = "Sat"
			
			
			# determine the month
			var month_of_year:int = date.month % 13
			# if the date was negative, wrap it around
			if month_of_year < 0:
				month_of_year += 12
			
			var month_name:String
			match month_of_year:
				1:
					month_name = "Jan"
				2:
					month_name = "Feb"
				3:
					month_name = "Mar"
				4:
					month_name = "Apl"
				5:
					month_name = "May"
				6:
					month_name = "Jun"
				7:
					month_name = "Jul"
				8:
					month_name = "Aug"
				9:
					month_name = "Sep"
				10:
					month_name = "Oct"
				11:
					month_name = "Nov"
				12:
					month_name = "Dec"
			
			
			label.text = day_name + " " + month_name + "\n" + str(time)
		
		
	else:
		# for minutes add a floating zero to the
		# beginning if it is less than 10
		if time >= 10 || time_type != "Minute":
			label.text = str(time)
		else:
			label.text = "0" + str(time)


func get_time() -> int:
	return time
func get_date() -> Time:
	return date

func _on_Up_Button_pressed():
	time += button_increment
	
	if time_type == "Day":
		# this counter is only used for
		# counting days and keeping track
		# of what day of the week it is
		total_numb_of_day_diff += button_increment
		date.day += 1
	
	if time > max_time:
		time = min_time
		
		# update the new max_time
		# based on the month and year
		if time_type == "Day":
			
			date.month += 1
			
			if date.month == 13:
				date.year += 1
				date.month = 0
			
			if Time_Helper.is_leap_year(date.year) && date.month == 2:
				max_time = 29
			else:
				max_time = Time_Helper.month_days[date.month]
				time = 1
			
	
	update_label()

func _on_Down_Button_pressed():
	time -= button_increment
	
	if time_type == "Day":
		# this counter is only used for
		# counting days and keeping track
		# of what day of the week it is
		total_numb_of_day_diff -= button_increment
		date.day -= 1
	
	if time < min_time:
		time = max_time
		
		# update the new max_time
		# based on the month and year
		if time_type == "Day":
			date.month -= 1
			
			if date.month == 0:
				date.year -= 1
				date.month = 12
			
			if Time_Helper.is_leap_year(date.year) && date.month == 2:
				max_time = 29
			else:
				max_time = Time_Helper.month_days[date.month]
				time = max_time
	
	update_label()
