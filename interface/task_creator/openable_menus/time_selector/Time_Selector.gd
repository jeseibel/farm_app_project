class_name Time_Selector

extends Openable_Menu


onready var relative_selector:ColorRect = $Background_Panel/Relative_Time_Selector
onready var absolute_selector:ColorRect = $Background_Panel/Absolute_Time_Selector


export(bool) var default_to_absolute_view:bool = false
var currently_showing_absolute_view:bool = default_to_absolute_view

onready var mode_button:Button = $Background_Panel/Mode_Button

onready var absolute_date:Time_Control = $Background_Panel/Absolute_Time_Selector/Day_Control
onready var absolute_hour:Time_Control = $Background_Panel/Absolute_Time_Selector/Hour_Control
onready var absolute_minute:Time_Control = $Background_Panel/Absolute_Time_Selector/Minute_Control
onready var absolute_am_pm:Time_Control = $Background_Panel/Absolute_Time_Selector/AM_PM_Control

onready var relative_hour:Time_Control = $Background_Panel/Relative_Time_Selector/Hour_Control
onready var relative_minute:Time_Control = $Background_Panel/Relative_Time_Selector/Minute_Control

onready var popup:Confirmation_Popup = $Confirmation_Popup


var button_absolute_text:String = "At (Hr:Min)"
var button_relative_text:String = "In (Hr:Min)"

func _ready():
	
	# show the correct starting menu
	show_menu_and_update_button(default_to_absolute_view)
	
	mode_button.text = button_relative_text

func set_icon_texture(icon:Texture) -> void:
	menu_button.set_icon_texture(icon)


func _on_Relative_CheckButton_toggled(button_pressed:bool):
	show_menu_and_update_button(button_pressed)

func show_menu_and_update_button(show_absolute:bool) -> void:
	if show_absolute:
		show_absolute_selector()
		mode_button.text = button_absolute_text
		mode_button.pressed = show_absolute
		currently_showing_absolute_view = true
	else:
		show_relative_selector()
		mode_button.text = button_relative_text
		mode_button.pressed = show_absolute
		currently_showing_absolute_view = false


func show_relative_selector() -> void:
	relative_selector.show()
	absolute_selector.hide()

func show_absolute_selector() -> void:
	relative_selector.hide()
	absolute_selector.show()


# will always return an absolute time
func get_time() -> Time:
	var time:Time = Time.new()
	
	if currently_showing_absolute_view:
		time.year = absolute_date.get_date().year
		time.month = absolute_date.get_date().month
		time.day = absolute_date.get_date().day
		
		time.hour = absolute_hour.get_time()
		time.minute = absolute_minute.get_time()
		
		if absolute_am_pm.get_time() == absolute_am_pm.MORNING:
			# do nothing
			pass
		elif absolute_am_pm.get_time() == absolute_am_pm.AFTERNOON:
			time.hour += 12
		else:
			print("ERROR: Time_Selector get_time absolute_am_pm returned " + str(absolute_am_pm.get_time()))
	else:
		# convert the time to absolute
		time.hour = relative_hour.get_time()
		time.minute = relative_minute.get_time()
		time.relative = true
		
		Time_Helper.convert_to_absolute(time)
	
	
	return time



# make sure that the time selected isn't 
# in the past
func _on_time_Done_Button_pressed():
	
	# is the time in the past?
	if Time_Helper.is_in_the_past(get_time()):
		# make sure that is what the user wants
		popup.show()
	else:
		._on_Done_Button_pressed()


func _on_Popup_yes_button_pressed():
	._on_Done_Button_pressed()

