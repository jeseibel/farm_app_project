class_name Field_Painter

extends Openable_Menu


onready var field_button:Button = $Field_Button
onready var canvas:TextureRect = $Field_Button/Canvas
onready var field_image:TextureRect = $Field_Button/Field_TextureRect

onready var clear_button:Button = $Clear_Button
onready var size_button:Button = $Size_Button
onready var color_button:Button = $Color_Button
onready var draw_button:Button = $Panel/Draw_Button
onready var done_button:Button = $Panel/Done_Button
onready var map_view_button:Button = $Panel/Map_View_Button


# the commented code can be used to use the screen size for the image
#onready var texture_size:int = min(canvas.rect_size.x, canvas.rect_size.y) as int
var texture_size:int = 640

# textures for the map button
var map_texture:Texture
var satellite_texture:Texture

# textures for the draw_erase_button
var drawing:bool = true
export(Texture) var pencil_button_texture
export(Texture) var eraser_button_texture
# textures for the map_stellite_button
var map_view:bool = true
export(Texture) var map_button_texture
export(Texture) var satellite_button_texture

# drawing variables
export(float) var paintbrush_size = 0.075
export(float) var paintbrush_variable_amount = 0.025
# dynamically scale the paintbrush with the canvas
onready var drawing_width:int = max( texture_size * paintbrush_size, 2) as int

var paint_brush_setting:int = 1
export(Texture) var small_painter_brush_texture
export(Texture) var medium_painter_brush_texture
export(Texture) var large_painter_brush_texture


var draw_color
enum painting_color{ORANGE, GREEN, BLACK, WHITE}
export(painting_color) var default_painting_color = painting_color.ORANGE
var color_setting:int = default_painting_color
export(Color) var orange_draw_color = Color(1,0.8,0.2,0.85)
export(Color) var green_draw_color = Color(0.2,0.8,0.1,0.85)
export(Color) var black_draw_color = Color(0,0,0 ,0.85)
export(Color) var white_draw_color = Color(1,1,1,0.85)

const erase_color = Color(0,0,0,0)
var pressing_on_field:bool = false

var canvas_image:Image





func _ready():
	
	var error:int = Config_Handler.connect("option_changed", self, "on_option_changed")
	if error != OK:
		print("ERROR: Config_Handler -> Task_Panel option_changed connection error code: " + str(error))
	
	
	# set up the buttons
	draw_button.icon = pencil_button_texture
	map_view_button.icon = map_button_texture
	
	# clear the field image
	field_image.texture = null
	
	# set the default color
	set_drawing_color(default_painting_color)
	set_color_button_font_color(default_painting_color)
	
	# default constructor
	init("title", load("res://images/fields/Far_North.png"), null, null)


func init(new_title:String, new_map_texture:Texture, new_satellite_texture:Texture, new_canvas_image:Image = null) -> void:
	
	set_title(new_title)
	
	# set the given textures for later use
	map_texture = new_map_texture
	satellite_texture = new_satellite_texture
	
	# set wether to have the satellite or map view
	map_view = Config_Handler.get_info().show_field_map_view_by_default
	
	# set the correct background texture and button image
	if map_view:
		map_view_button.icon = map_button_texture
		field_image.texture = map_texture
	else:
		map_view_button.icon = satellite_button_texture
		field_image.texture = satellite_texture
	
	
	# set up the canvas
	if new_canvas_image == null:
		canvas_image = Image.new()
		
		
		canvas_image.create(texture_size, texture_size, false, Image.FORMAT_RGBA8)
		
		canvas_image.fill(Color(0,0,0,0))
	else:
		canvas_image = new_canvas_image
	
	
	var itex = ImageTexture.new()
	itex.create_from_image(canvas_image)
	canvas.texture = itex
	
	
	# enable the buttons now that a canvas has been created
	draw_button.disabled = false
	clear_button.disabled = false
	map_view_button.disabled = false
	done_button.disabled = false


func _draw():
	if canvas_image != null:
		# update the texture
		var itex = ImageTexture.new()
		itex.create_from_image(canvas_image)
		
		canvas.texture = itex




func _input(event:InputEvent) -> void:
	
	# only paint if the user is pressing on the field
	if pressing_on_field:
		 
		if event is InputEventScreenTouch:
			# only paint if the canvas exists
			if canvas_image != null:
				# the fancy math is to convert canvas pixels into 
				# image pixels, since the two are different
				var minimum:float = min(canvas.rect_size.x, canvas.rect_size.y)
				var pixel_density:float = minimum / texture_size
				paint_canvas(canvas.get_local_mouse_position() / pixel_density)
		
		if event is InputEventScreenDrag:
			# only paint if the canvas exists
			if canvas_image != null:
				
				var minimum:float = min(canvas.rect_size.x, canvas.rect_size.y)
				var pixel_density:float = minimum / texture_size
				paint_canvas(canvas.get_local_mouse_position() / pixel_density)

# the position given should be relative to the canvas
func paint_canvas(position:Vector2) -> void:
	
	# touch position relative to the canvas
	var center_x:int = position.x as int
	var center_y:int = position.y as int
	
	# determine where the canvas image is in relation to the canvas itself
	var minimum:int = min(canvas.rect_size.x, canvas.rect_size.y) as int
		# the pixel density is required if the canvas texture is a different size then
		# the canvas itself
	var pixel_density:float = minimum as float / texture_size as float
	var offset_x:int = (((canvas.rect_size.x / pixel_density) - texture_size) / 2) as int
	var offset_y:int = (((canvas.rect_size.y / pixel_density) - texture_size) / 2) as int
	
	# make sure to only paint inside the screen
	if (center_x < 0 + offset_x ||
	center_y < 0 + offset_y ||
	center_x > texture_size + offset_x || 
	center_y > texture_size + offset_y):
		return
	
	canvas_image.lock()
	# paint the image
	for x in range(-drawing_width/2,drawing_width/2):
		for y in range(-drawing_width/2,drawing_width/2):
			var new_x:int = center_x + x - offset_x
			var new_y:int = center_y + y - offset_y
			
			# make sure to only paint inside the image boundries
			if (new_x >= 0 && new_y >= 0 &&
			new_x < canvas_image.get_size().x && new_y < canvas_image.get_size().y):
				if drawing:
					canvas_image.set_pixel(new_x,new_y, draw_color)
				else:
					canvas_image.set_pixel(new_x,new_y, erase_color)
	
	canvas_image.unlock()
	
	update()








func get_canvas_Image() -> Image:
	
#	shouldn't be needed, but just in case
#	var size:Vector2 = canvas_image.get_size()
#	var minimum:int = min(size.x, size.y) as int
#
#	var return_image:Image = canvas_image
#	return_image.crop(minimum, minimum)
	
	return canvas_image




func set_drawing_color(color_code:int) -> void:
	
	match color_code:
		painting_color.ORANGE:
			draw_color = orange_draw_color
		painting_color.GREEN:
			draw_color = green_draw_color
		painting_color.WHITE:
			draw_color = white_draw_color
		painting_color.BLACK:
			draw_color = black_draw_color

func set_color_button_font_color(new_color_setting:int) -> void:
	
	match new_color_setting:
		painting_color.ORANGE:
			color_button.text = "O"
			color_button.add_color_override("font_color", orange_draw_color)
			color_button.add_color_override("font_color_hover", orange_draw_color)
		painting_color.GREEN:
			color_button.text = "G"
			color_button.add_color_override("font_color", green_draw_color) 
			color_button.add_color_override("font_color_hover", green_draw_color)
		painting_color.WHITE:
			color_button.text = "W"
			color_button.add_color_override("font_color", white_draw_color) 
			color_button.add_color_override("font_color_hover", white_draw_color)
		painting_color.BLACK:
			color_button.text = "B"
			color_button.add_color_override("font_color", black_draw_color)
			color_button.add_color_override("font_color_hover", black_draw_color) 




func _on_Draw_Erase_Button_pressed():
	if drawing:
		draw_button.icon = eraser_button_texture
	else:
		draw_button.icon = pencil_button_texture
	
	drawing = !drawing

func _on_Map_Satellite_Button_pressed():
	if map_view:
		map_view_button.icon = satellite_button_texture
		field_image.texture = satellite_texture
	else:	
		map_view_button.icon = map_button_texture
		field_image.texture = map_texture
	
	map_view = !map_view

func _on_Clear_Button_pressed():
	# only clear if the canvas exists
	if canvas_image != null:
		canvas_image.fill(Color(0,0,0,0))
		update()





func _on_Field_Button_button_up():
	pressing_on_field = false
func _on_Field_Button_button_down():
	pressing_on_field = true




func on_option_changed(option_name:String, option):
	if option_name == "show_field_map_view_by_default":
		if option is bool:
			map_view = option as bool
			
			if map_view:
				map_view_button.icon = map_button_texture
				field_image.texture = map_texture
			else:
				map_view_button.icon = satellite_button_texture
				field_image.texture = satellite_texture
			
		else:
			print("ERROR: show_field_map_view_by_default was given a non boolean option")



func _on_Size_Button_pressed():
	paint_brush_setting += 1
	
	# cycle between 1, 2, 0
	if paint_brush_setting > 2:
		paint_brush_setting = 0
	
	
	var new_size:float
	
	if paint_brush_setting == 0:
		new_size = paintbrush_size - paintbrush_variable_amount
		size_button.icon = small_painter_brush_texture
	
	elif paint_brush_setting == 1:
		new_size = paintbrush_size
		size_button.icon = medium_painter_brush_texture
	
	elif paint_brush_setting == 2:
		new_size = paintbrush_size + paintbrush_variable_amount
		size_button.icon = large_painter_brush_texture
	
	drawing_width = max( texture_size * new_size, 2) as int



func _on_Color_Button_pressed():
	
	color_setting += 1
	
	if color_setting > painting_color.WHITE:
		color_setting = painting_color.ORANGE
	
	set_color_button_font_color(color_setting)
	set_drawing_color(color_setting)
