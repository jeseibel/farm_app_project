class_name Openable_Menu

extends Panel

signal menu_button_pressed(this)
signal done_button_pressed(this)

onready var menu_button:Icon_Button = $Menu_Button


var parent_exp_menu:Expanding_Menu
var parent_button:Button




func set_title(new_title:String) -> void:
	menu_button.set_text(new_title)

func get_title() -> String:
	return menu_button.get_text()


func set_icon_texture(icon:Texture) -> void:
	menu_button.set_icon_texture(icon)
func hide_icon_texture() -> void:
	menu_button.hide_icon_texture()

func get_icon_texture() -> Texture:
	return menu_button.get_icon_texture()


###########
# signals #
###########

func _on_Menu_Button_pressed():
	emit_signal("menu_button_pressed", self)

func _on_Done_Button_pressed():
	emit_signal("done_button_pressed", self)
