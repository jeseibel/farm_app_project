class_name Expanding_Menu

extends Panel

const menu_type:GDScript = preload("res://interface/task_creator/Menu_Type.gd")

const resizable_button:PackedScene = preload("res://interface/general/resizable_button/Resizable_Button.tscn")


signal show_field_painter(resizable_button)
signal show_placement_selector(menu_title)
signal show_text_box(this)
signal selected_items_changed(this)


onready var scroll_container:ScrollContainer = $ScrollContainer
onready var menu:VBoxContainer = $ScrollContainer/VBoxContainer
onready var resize_button:Icon_Button = $resize_button
onready var icon:TextureRect = $resize_button/TextureRect


var default_menu_type:int

# this array holds the text of each button that
#  is currently selected
var selection_array:Array = []

# these variables should not be used outside of _ready(),
# they are set when a method that requires access to a node
# is called before _ready()
var tmp_title:String = ""
var tmp_max_height:float = 0
var tmp_item_type:int = 0
var tmp_items:Array = []

# vars used for minimizing and maximizing the menu
var menu_open:bool
var menu_max_height:float
var menu_min_height:float

var scroll_max_height:float
var scroll_min_height:float

var resize_button_min_anchor:float
var resize_button_max_anchor:float







func _ready():
	# set up the initial variables for 
	# minimizing and maximizing
	menu_min_height = resize_button.rect_size.y
	menu_max_height = get_viewport().size.y
	
	scroll_max_height = scroll_container.rect_size.y
	scroll_min_height = 0
	
	resize_button_min_anchor = 1
	resize_button_max_anchor = resize_button.anchor_bottom
	
	
	menu_open = true
	
	# if on a desktop, add a small deadzone
	# to help with clicking on things in a scrollpane
	if OS.get_name() == "Android" || OS.get_name() == "Windows" || OS.get_name() == "OSX" || OS.get_name() == "UWP":
		scroll_container.scroll_deadzone = 8
	
	# if init was called before, call it now
	if tmp_max_height != 0 || tmp_items.size() != 0:
		init(tmp_max_height, tmp_item_type, tmp_items)

	# if set_title was called before, call it now
	if !tmp_title.empty():
		set_title(tmp_title)


func init(new_menu_max_height:float, new_menu_item_type:int, items:Array) -> void:	
	# this method was called before _ready was called,
	# save what would have been set to so they can be set
	# during the _ready method
	if menu == null:
		tmp_max_height = new_menu_max_height
		tmp_items = items
		
		return
	
	menu_max_height = new_menu_max_height
	default_menu_type = new_menu_item_type
	
	add_items_to_menu(items)
	
	hide_menu()

func add_items_to_menu(items:Array, menu_item_type:int = default_menu_type) -> void:
	var button_group:ButtonGroup = ButtonGroup.new()
	
	var item_type:int = menu_item_type
	
	# add each of the items to the menu
	for it in items:
		
		var item_text:String = str(it)
		
		# if this item has a different type than its parent
		# override it
		if menu_type.determine_item_type(it) != menu_type.UNKOWN:
			item_type = menu_type.determine_item_type(it)
			item_text = menu_type.remove_special_type_text(item_text)
		else:
			# this item doesn't have a set type,
			# use the parent's
			item_type = menu_item_type
		
		
		
		# add the item
		
		if item_type == menu_type.CHECK_BUTTON:
			var new_button:Resizable_Button = resizable_button.instance()
			new_button.set_text(item_text)
			
			var error:int = new_button.connect("toggled",self,"_on_option_button_pressed", [ new_button ])
			if error != 0:
				print("expanding menu button pressed connection error code: " + str(error))
			
			menu.add_child(new_button)
			
			
		elif item_type == menu_type.RADIO_BUTTON:
			var new_button:Resizable_Button = resizable_button.instance()
			menu.add_child(new_button)
			
			new_button.set_text(item_text)
			new_button.make_radio_button(button_group)
			
			var error:int = new_button.connect("toggled",self,"_on_option_button_pressed", [ new_button ])
			if error != 0:
				print("expanding menu button pressed connection error code: " + str(error))
			
			
		elif item_type == menu_type.PLACEMENT_SELECTOR:
			var new_button:Resizable_Button = resizable_button.instance()
			menu.add_child(new_button)
			
			new_button.set_text(item_text)
			new_button.make_radio_button(button_group)
			
			var error:int = new_button.connect("pressed",self,"show_placement_selector", [ new_button ])
			if error != 0:
				print("show_placement_selector button_pressed connection error code: " + str(error))
			
			
		elif item_type == menu_type.FIELD_PAINTER:
			var new_button:Resizable_Button = resizable_button.instance()
			menu.add_child(new_button)
			
			new_button.set_text(item_text)
			new_button.make_radio_button(button_group)
			
			var error:int = new_button.connect("pressed",self,"show_field_painter", [ new_button ])
			if error != 0:
				print("show_field_painter pressed_text connection error code: " + str(error))
			
			
		elif item_type == menu_type.TEXT_BOX:
			var new_button:Resizable_Button = resizable_button.instance()
			menu.add_child(new_button)
			
			new_button.set_text(item_text)
			new_button.make_radio_button(button_group)
			
			var error:int = new_button.connect("pressed",self,"show_text_box", [ new_button ])
			if error != 0:
				print("show_text_box pressed_text connection error code: " + str(error))
			
			
		else:
			print("ERROR: item_type: " + str(item_type) + " wasn't added to the Expanding_Menu")
	
	
	# fake disable scrolling on menus that aren't long enough to require scrolling
	# if we don't do this scrolling through items will feel sticky
	# (we can't disable scrolling since the scroll pane is what allows us to hide
	# the menu)
	
	var height:float = 0
	
	for child in menu.get_children():
		if child is Control:
			height += child.rect_size.y
	
	if height < scroll_max_height:
		scroll_container.scroll_deadzone = 10000
	else:
		scroll_container.scroll_deadzone = 0







# set the title of the minimize/maximize button
func set_title(new_title:String) -> void:
	# this method was called before _ready was called
	# save the title, so it can be set
	# during the _ready method
	if resize_button == null:
		tmp_title = new_title
		
		return
	
	resize_button.set_text(new_title)

func get_title() -> String:
	return resize_button.get_text()


func set_icon_texture(new_texture:Texture) -> void:
	icon.texture = new_texture

func get_icon_texture() -> Texture:
	return icon.texture


func get_resize_button() -> Icon_Button:
	return resize_button



func enable() -> void:
	resize_button.disabled = false

func disable() -> void:
	resize_button.disabled = true





# this is called when a button in the menu is pressed
func _on_option_button_pressed(pressed:bool, button:Resizable_Button):
	add_or_remove_selection_item(pressed, button.get_text())

# add or remove new_item from the selection_array
func add_or_remove_selection_item(add_item:bool, new_item:String) -> void:
	
	# go through the array of items and try to find the new item
	for i in range(0,selection_array.size()):
		if selection_array[i] is String:
			var s = selection_array[i] as String
			
			if s == new_item && !add_item:
				# remove the item from the array and stop
				selection_array.remove(i)
				return
			elif s == new_item && add_item:
				# we found the item we were supposed to add
				# don't add it again, just stop
				return
	
	# we went through the whole array without finding new_item
	# append it to the list
	selection_array.append(new_item)
	
	emit_signal("selected_items_changed", self)

# return the selection_array to see what objects
# are currently selected
func get_selected_objects() -> Array:
	return selection_array

func clear_selected_objects() -> void:
	selection_array.clear()

# remove and free all children of menu
func clear_menu() -> void:
	clear_selected_objects()
	
	for item in menu.get_children():
		
		menu.remove_child(item)
		
		# if for some reason a null pointer
		# was a child don't try to free it
		if item != null:
			item.queue_free()




func _on_min_max_button_pressed() -> void:
	if menu_open:
		hide_menu()
	else:
		show_menu()

func hide_menu() -> void:
	# shrink the main panel so it only fits the resize button
	rect_min_size = Vector2(0,menu_min_height)
	rect_size = Vector2(rect_size.x,menu_min_height)
	
	# have the resize button fill the whole panel
	resize_button.anchor_bottom = resize_button_min_anchor
	
	# shrink the scroll menu to the minimum height
	# (which by default is 0)
	scroll_container.rect_min_size = Vector2(0,scroll_min_height)
	scroll_container.rect_size = Vector2(scroll_container.rect_size.x,scroll_min_height)
	
	# the menu is now closed
	menu_open = false

func show_menu() -> void:
	
	# maxamize the main panel
	rect_min_size = Vector2(0,menu_max_height)
	
	# have the resize button fill its original amount
	# in the main panel
	resize_button.anchor_bottom = resize_button_max_anchor
	
	# maximize the scroll container
	scroll_container.rect_min_size = Vector2(0,scroll_max_height)
	
	# set the theme and size of the children
	var theme:Theme = self.theme
	for child in menu.get_children():
		child.theme = theme
	
	# the menu is now open
	menu_open = true

func show_field_painter(button:Resizable_Button) -> void:
	emit_signal("show_field_painter", button)

func show_placement_selector(button:Resizable_Button) -> void:
	emit_signal("show_placement_selector", button.get_text())

func show_text_box(button:Resizable_Button) -> void:
	emit_signal("show_text_box", self, button)
