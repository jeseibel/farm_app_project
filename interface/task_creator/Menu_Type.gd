extends Node

enum {UNKOWN = -1, RADIO_BUTTON = 0, CHECK_BUTTON,
 TIME_SELECTOR, FIELD_PAINTER, PLACEMENT_SELECTOR, TEXT_BOX}


# returns the enum of 
static func determine_item_type(text:String) -> int:
	
	var index:int = text.find("SHOW_FIELD_PAINTER")
	
	if index != -1:
		text = text.substr(0, index - 1) # the -1 is to account for the space before
		return FIELD_PAINTER
	
	
	index = text.find("SHOW_PLACEMENT_SELECTOR")
	
	if index != -1:
		text = text.substr(0, index - 1) # the -1 is to account for the space before
		return PLACEMENT_SELECTOR
	
	
	index = text.find("SHOW_TIME_SELECTOR")
	
	if index != -1:
		text = text.substr(0, index - 1) # the -1 is to account for the space before
		return TIME_SELECTOR
	
	
	index = text.find("RADIO_BUTTON")
	
	if index != -1:
		text = text.substr(0, index - 1) # the -1 is to account for the space before
		return RADIO_BUTTON
	
	
	index = text.find("SHOW_TEXT_BOX")
	
	if index != -1:
		text = text.substr(0, index - 1) # the -1 is to account for the space before
		return TEXT_BOX
	
	
	# no special type was found
	return UNKOWN



# should be called AFTER determine_item_type, never before
# otherwise determine_item_type won't find anything
static func remove_special_type_text(text:String) -> String:
	
	var index:int = text.find("SHOW_FIELD_PAINTER")
	
	if index != -1:
		text = text.substr(0, index - 1)
		return text
	
	
	index = text.find("SHOW_PLACEMENT_SELECTOR")
	
	if index != -1:
		text = text.substr(0, index - 1)
		return text
	
	
	index = text.find("SHOW_TIME_SELECTOR")
	
	if index != -1:
		text = text.substr(0, index - 1)
		return text
	
	
	index = text.find("RADIO_BUTTON")
	
	if index != -1:
		text = text.substr(0, index - 1)
		return text
	
	
	index = text.find("SHOW_TEXT_BOX")
	
	if index != -1:
		text = text.substr(0, index - 1)
		return text
	
	# nothing was found, return the text unedited
	return text
