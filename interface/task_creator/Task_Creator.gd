extends Control

class_name Task_Creator

signal closed()

const Menu_Type:GDScript = preload("res://interface/task_creator/Menu_Type.gd")

const expanding_menu:PackedScene = preload("res://interface/task_creator/expanding_menu/Expanding_Menu.tscn")
const icon_button:PackedScene = preload("res://interface/general/icon_button/Icon_Button.tscn")

export(Theme) var incomplete_theme:Theme
export(Theme) var complete_theme:Theme
export(Theme) var positive_button_theme:Theme

onready var title_label:Label = $Title_Background/Title
onready var scroll_container:ScrollContainer =  $ScrollContainer
onready var menu:VBoxContainer = $ScrollContainer/VBoxContainer

onready var field_painter:Field_Painter = $Openable_Menus/Field_Painter
onready var placement_selector:Placement_Selector = $Openable_Menus/Placement_Selector
onready var time_selector:Time_Selector = $Openable_Menus/Time_Selector
onready var text_box:Text_Box = $Openable_Menus/Text_Box



var done_button:Button
var attatchment_menu:Expanding_Menu

var time_selector_button:Icon_Button
var placement_selector_button:Icon_Button


var vehicle_info:Dictionary
var vehicle_attatchments_by_tag:Dictionary

var field:Field
var field_painter_canvas_image:Image = null

var enable_auto_scrolling = false
var default_button_size:Vector2

var task:Task = null

var connected_users:Dictionary = {}

var exit_popup_text = "Are you sure you want to quit?"
onready var popup:Confirmation_Popup = $Confirmation_Popup






########################
# constructors / setup #
########################


# Called when the node enters the scene tree for the first time.
func _ready():
	
	var error:int = Config_Handler.connect("option_changed", self, "on_option_changed")
	if error != OK:
		print("ERROR: Config_Handler -> Task_Panel option_changed connection error code: " + str(error))
	
	enable_auto_scrolling = Config_Handler.get_info().enable_auto_scrolling
	
	# if on a desktop, add a small deadzone
	# to help with clicking on things in a scrollpane
	if OS.get_name() == "Android" || OS.get_name() == "Windows" || OS.get_name() == "OSX" || OS.get_name() == "UWP":
		scroll_container.scroll_deadzone = 8
	
	# To get the size of the button an Expanding_Menu must be added
	# so that it scales relative to the task_creator
	var tmp_menu:Expanding_Menu = expanding_menu.instance()
	add_child(tmp_menu)
	default_button_size = tmp_menu.get_resize_button().rect_size
	remove_child(tmp_menu)
	
	# set the text for the exit popup
	popup.set_text(exit_popup_text)

# set up the Task creator for use, reseting
# any variables that may have been set before
func init(new_field:Field) -> void:
	
	# "connected_users" -> Array of Dictionaries
	# "task_order" -> Array
	# "task_options" -> Dictionary
	# "task_aliases" -> Dictionary
	# "task_icon_locations" -> Dictionary
	# "vehicle_info" -> Dictionary
	# "vehicle_attatchments_by_tag" -> Dcitionary
	var task_data:Dictionary = Network.get_task_data()
	
	task = Task.new()
	task.create_data_aliases(task_data.task_aliases)
	
	var error:int = task.connect("complete", self, "enable_done_button")
	if error != 0:
		print("task complete connection error code: " + str(error))
	
	
	# build the who options from the connected_users
	connected_users = Network.get_connected_users().duplicate()
	for device_id in connected_users:
		task_data.task_options.who.append(connected_users[device_id].name)
	
	
	field = new_field
	task.field = new_field
	
	vehicle_info = task_data.vehicle_info
	vehicle_attatchments_by_tag = task_data.vehicle_attatchments_by_tag
	
	
	# get the task_icons from their loctaions
	var task_icons:Dictionary = {}
	
	for icon_name in task_data.task_icon_locations:
		task_icons[icon_name] = load(task_data.task_icon_locations[icon_name])
	
	
	set_title(field.name)
	
	populate_menu(task_data.task_order, task_data.task_options, task_icons)



# takes in a 2D array
func populate_menu(task_option_order:Array, task_options:Dictionary, task_icons:Dictionary) -> void:
	
	# remove everything from the menu so we can start
	# with a clean slate
	for child in menu.get_children():
		menu.remove_child(child)
		child.queue_free()
	
	# reset the size of the menu
	# (If we don't do this the menu will become larger
	# every time the populate_menu method is run)
	menu.rect_size = scroll_container.rect_size
	
	
	var options:Array = []
	var option_icons:Array = []
	
	# put the options in order before adding them to the menu
	for item in task_option_order:
		options.append(task_options[item])
		option_icons.append(task_icons[item])
	
	
	var numb_of_menus:int = options.size()
	
	# add each menu
	for option_index in range(0,numb_of_menus):
		
		var parent_item_type:int = 0
		
		# determine what type of data should be displayed in this menu
		parent_item_type = Menu_Type.determine_item_type(options[option_index][0])
		# remove the item_type text
		options[option_index][0] = Menu_Type.remove_special_type_text(options[option_index][0])
		
		
		# set the defualt button type to radio
		if parent_item_type == Menu_Type.UNKOWN:
			parent_item_type = Menu_Type.RADIO_BUTTON
		
		if parent_item_type == Menu_Type.PLACEMENT_SELECTOR:
			add_field_placement_button(str(options[option_index][0]), option_icons[option_index])
		elif parent_item_type == Menu_Type.TIME_SELECTOR:
			add_time_selector_button(str(options[option_index][0]), option_icons[option_index])
		elif parent_item_type == Menu_Type.TEXT_BOX:
			add_text_box_button(str(options[option_index][0]), option_icons[option_index])
		else:
			# by default items will be added via an expanding menu
			add_expanding_menu(options, option_index, parent_item_type, option_icons[option_index])
	
	
	
	
	# create the done button
	done_button = Button.new()
	
	done_button.text = "DONE"
	
	var error:int = done_button.connect("pressed", self, "_on_done_button_pressed")
	if error != 0:
		print("task complete connection error code: " + str(error))
	
	done_button.disabled = true 
	done_button.theme = positive_button_theme
	done_button.rect_min_size = Vector2(0, default_button_size.y)
	done_button.mouse_filter = MOUSE_FILTER_PASS
	done_button.focus_mode = Control.FOCUS_NONE
	
	menu.add_child(done_button)
	
	
	# Add a spacer at the bottom so that the scroll container
	# can move down after each item has been completed
	var control:Control = Control.new()
	
	control.rect_min_size = Vector2(0, menu.rect_size.y - default_button_size.y)
	control.mouse_filter = MOUSE_FILTER_IGNORE
	
	menu.add_child(control)
	
	
	
	return



func add_field_placement_button(menu_text:String, icon:Texture):
	placement_selector_button = icon_button.instance()
	menu.add_child(placement_selector_button)
	
	placement_selector_button.set_icon_texture(icon)
	
	placement_selector_button.mouse_filter = MOUSE_FILTER_PASS
	placement_selector_button.set_text(menu_text)
	placement_selector_button.theme = incomplete_theme
	placement_selector_button.rect_size = default_button_size
	# if this isn't true then scaling down doesn't work correctly
	placement_selector_button.clip_text = true

	var error:int = placement_selector_button.connect("pressed",self,"show_field_placement_selector",[ menu_text ])
	if error != 0:
		print("field_placement_button show_field_placement_selector connection error code: " + str(error))

func add_time_selector_button(menu_text:String, icon:Texture):
	time_selector_button = icon_button.instance()
	menu.add_child(time_selector_button)
	
	time_selector_button.set_icon_texture(icon)
	time_selector.set_icon_texture(icon)
	
	time_selector_button.mouse_filter = MOUSE_FILTER_PASS
	time_selector_button.set_text(menu_text)
	time_selector_button.theme = incomplete_theme
	time_selector_button.rect_size = default_button_size
	# if this isn't true then scaling down doesn't work correctly
	time_selector_button.clip_text = true
	
	var error:int = time_selector_button.connect("pressed",self,"show_time_selector",[ time_selector_button ])
	if error != 0:
		print("field_placement_button show_field_placement_selector connection error code: " + str(error))

func add_text_box_button(menu_text:String, icon:Texture):
	var new_button:Icon_Button = icon_button.instance()
	menu.add_child(new_button)
	
	new_button.set_icon_texture(icon)
	
	new_button.mouse_filter = MOUSE_FILTER_PASS
	new_button.set_text(menu_text)
	new_button.theme = complete_theme
	new_button.rect_size = default_button_size
	# if this isn't true then scaling down doesn't work correctly
	new_button.clip_text = true

	var error:int = new_button.connect("pressed",self,"show_text_box", [ new_button, null ])
	if error != 0:
		print("Resizable_Button show_text_box connection error code: " + str(error))


# options is a 2D array
func add_expanding_menu(options:Array, option_index:int, parent_item_type:int, icon:Texture):
	
	var item_type:int = parent_item_type
	var new_menu = expanding_menu.instance()
	menu.add_child(new_menu)
	
	
	new_menu.set_icon_texture(icon)
	new_menu.theme = incomplete_theme
	
	var error:int = new_menu.connect("show_field_painter",self,"show_field_painter", [ new_menu ])
	if error != 0:
		print("expanding_menu show_field_painter connection error code: " + str(error))
	
	error = new_menu.connect("show_placement_selector",self,"show_field_placement_selector")
	if error != 0:
		print("expanding_menu show_placement_selector connection error code: " + str(error))
	
	error = new_menu.connect("selected_items_changed",self,"on_selected_items_changed")
	if error != 0:
		print("expanding_menu selected_items_changed connection error code: " + str(error))
	
	error = new_menu.connect("show_text_box",self,"show_expanding_text_box")
	if error != 0:
		print("expanding_menu show_expanding_text_box connection error code: " + str(error))
	
	
	
	# set the menu's title
	new_menu.set_title(str(options[option_index][0]))
	
	if new_menu.get_title() == task.get_data_aliases()["attatchment"]:
		attatchment_menu = new_menu
		attatchment_menu.disable()
	
	var item_array:Array = []
	# add every item that isn't the title to the menu
	for j in options[option_index].size():
		if j == 0:
			# the first item is the title
			continue
		item_array.append(options[option_index][j])
	
	
	new_menu.init(scroll_container.rect_size.y, item_type, item_array)



func set_title(new_title:String) -> void:
	title_label.text = new_title


#########
# menus #
#########


func show_time_selector(button:Icon_Button) -> void:
	time_selector.set_title(button.get_text())
	time_selector.theme = time_selector_button.theme
	time_selector.set_icon_texture(time_selector_button.get_icon_texture())
	time_selector.show()
func _on_Time_Selector_menu_button_pressed(selector:Time_Selector):
	selector.hide()
func _on_Time_Selector_done_button_pressed(selector:Time_Selector):
	selector.hide()
	# returned as [hour, minute, am/pm (-1 means relative time) ]
	if task.add_data(time_selector.get_title() ,time_selector.get_time()) == task.NEW_DATA_ADDED:
		scroll_to_next_item(time_selector_button)
		time_selector_button.theme = complete_theme
		time_selector.theme = complete_theme

func show_field_painter(button:Resizable_Button, exp_menu:Expanding_Menu) -> void:
	field_painter.parent_exp_menu = exp_menu
	field_painter.parent_button = button
	field_painter.theme = field_painter.parent_exp_menu.theme
	field_painter.set_icon_texture(field_painter.parent_exp_menu.get_icon_texture())
	
	field_painter.init(button.get_text(), field.get_map_texture(), field.get_satellite_texture(), field_painter_canvas_image)
	field_painter.show()
func _on_Field_Painter_menu_button_pressed(painter:Field_Painter):
	painter.hide()
	field_painter_canvas_image = painter.get_canvas_Image()
	
	if painter.parent_button is Resizable_Button:
		var par:Resizable_Button = painter.parent_button as Resizable_Button
		par.pressed = false
		par._on_Button_toggled(par.pressed)
func _on_Field_Painter_done_button_pressed(painter:Field_Painter):
	painter.hide()
	field_painter_canvas_image = painter.get_canvas_Image()
	
	var goal:Goal = Goal.new()
	
	goal.name = painter.get_title()
	goal.image = painter.get_canvas_Image()
	
	painter.parent_exp_menu.hide_menu()
	
	var result:int = task.add_data(painter.parent_exp_menu.get_title(), goal)
	if result == task.NEW_DATA_ADDED:
		scroll_to_next_item(painter.parent_exp_menu)
		painter.parent_exp_menu.theme = complete_theme
		painter.parent_button.theme = complete_theme

func show_field_placement_selector(menu_title:String) -> void:
	placement_selector.theme = placement_selector_button.theme
	placement_selector.set_icon_texture(placement_selector_button.get_icon_texture())
	
	placement_selector.init(menu_title, field.get_map_texture(), field.get_satellite_texture(), task.parking_location, task.parking_direction, task.has_data(menu_title))
	placement_selector.set_painter_image(field_painter_canvas_image)
	
	placement_selector.show()
func _on_Placement_Selector_menu_button_pressed(selector:Placement_Selector):
	selector.hide()
func _on_Placement_Selector_done_button_pressed(selector:Placement_Selector):
	selector.hide()
	
	if task.add_data(selector.get_title(), [selector.get_parking_location(), selector.get_parking_rotation()]) == task.NEW_DATA_ADDED:
		scroll_to_next_item(placement_selector_button)
		placement_selector_button.theme = complete_theme

func show_text_box(button:Icon_Button, exp_menu:Expanding_Menu) -> void:
	text_box.set_title(button.get_text())
	text_box.theme = button.theme
	text_box.parent_button = button
	text_box.parent_exp_menu = exp_menu
	text_box.set_text_from_task(task)
	
	
	text_box.set_icon_texture(button.get_icon_texture())
	
	text_box.open()
func show_expanding_text_box(exp_menu:Expanding_Menu, button:Resizable_Button) -> void:
	text_box.set_title(exp_menu.get_title())
	text_box.theme = exp_menu.theme
	text_box.parent_exp_menu = exp_menu
	text_box.parent_button = button
	text_box.set_text_from_task(task)
	
	
	text_box.set_icon_texture(exp_menu.get_icon_texture())
	
	text_box.open()
func _on_Text_Box_menu_button_pressed(tb:Text_Box):
	tb.hide()
	
	add_text_box_data_to_task(tb)
	
	if tb.parent_button is Resizable_Button && tb.get_text().empty():
		var par:Resizable_Button = tb.parent_button as Resizable_Button
		par.pressed = false
		par._on_Button_toggled(par.pressed)
	
	text_box.parent_exp_menu = null
	text_box.parent_button = null
func _on_Text_Box_done_button_pressed(tb:Text_Box):
	tb.hide()
	
	add_text_box_data_to_task(tb)

func add_text_box_data_to_task(tb:Text_Box) -> void:
	
	if tb.get_text().length() == 0:
		# don't try to add any data, the text box was empty
		# deselect the button associated with this menu if applicable
		
		if tb.parent_button is Resizable_Button:
			var par:Resizable_Button = tb.parent_button as Resizable_Button
			par.pressed = false
			par._on_Button_toggled(par.pressed)
		
	else:
		# there is data, add it to the task and update both 
		# parents' theme
		
		if tb.parent_exp_menu is Expanding_Menu:
			var exp_menu:Expanding_Menu = tb.parent_exp_menu as Expanding_Menu
			
			exp_menu.hide_menu()
			on_selected_items_changed(exp_menu)
			exp_menu.clear_selected_objects()
			exp_menu.add_or_remove_selection_item(true, tb.get_text())
		
		if task.add_data(tb.get_title(), tb.get_text()) == task.NEW_DATA_ADDED:
			scroll_to_next_item(tb.parent_button)
			
			if tb.parent_exp_menu != null:
				tb.parent_exp_menu.theme = complete_theme
			if tb.parent_button != null:
				tb.parent_button.theme = complete_theme
			tb.parent_exp_menu = null
			tb.parent_button = null





#################
# other methods #
#################


func on_selected_items_changed(exp_menu:Expanding_Menu) -> void:
	exp_menu.theme = theme
	
	var title:String = exp_menu.get_title()
	var data_array:Array = exp_menu.get_selected_objects() as Array
	
	
	# don't continue if no data was given
	if data_array.empty():
		return
	
	
	if title == task.get_data_aliases()["vehicle"]:
		# check to see if there is a single selected vehicle
		
		if data_array.size() != 1:
			# the array is the wrong size, don't continue
			return 
		# elif array.size() == 1:
			# the array is the correct size, continue on
		
		# get the selected vehicle
		var vehicle:String = exp_menu.get_selected_objects()[0]
		
		update_attatchment_menu(vehicle)
		# remove the currently selected attatchment
		# warning-ignore:return_value_discarded
		task.remove_data(attatchment_menu.get_title())
		
		# always close the vehicle menu and show the attatchment menu
		# since the attatchment menu's items have just been updated
		exp_menu.hide_menu()
		
		scroll_to_next_item(exp_menu)
	
	
	
	var result:int = task.add_data(title,data_array)
	
	if result == task.NEW_DATA_ADDED:
		exp_menu.theme = complete_theme
		exp_menu.hide_menu()
		scroll_to_next_item(exp_menu)
	elif result == task.DATA_OVERWRITTEN:
		exp_menu.theme = complete_theme
		exp_menu.hide_menu()
	elif result == task.DATA_ADDING_FAILED:
		print("ERROR: task data adding failed")
	elif result == task.INCORRECT_DATA_TYPE_GIVEN:
		print("ERROR: incorrect data type given to the task object")


func update_attatchment_menu(new_vehicle:String) -> void:
	# since the previous selection will be removed,
	# don't allow the user to continue
	disable_done_button()
	# clear whatever may have been in the menu before
	attatchment_menu.clear_menu()
	# enable the menu just in case it wasn't before
	attatchment_menu.enable()
	# reset the theme, since the user has to re-select a attatchment
	attatchment_menu.theme = incomplete_theme
	
	
	
	var new_items:Array = []
	
	# get this vehicle's tags
	var vehicle_tags:Array = []
	# check if there is a dictionary entry for this vehicle
	if vehicle_info.has(new_vehicle):
		vehicle_tags = vehicle_info[new_vehicle][0]
	
	
	# add the attatchments shared by all vehicles
	for item in vehicle_attatchments_by_tag["all"]:
		new_items.append(item)
	
	# add the items for vehicles with the given tags
	for tag in vehicle_tags:
		for item in vehicle_attatchments_by_tag[tag]:
			new_items.append(item)
	
	# check if there is a dictionary entry for this vehicle
	# and if so, add the items for this specific vehicle
	if vehicle_info.has(new_vehicle):
		for item in vehicle_info[new_vehicle][1]:
			new_items.append(item)
	
	
	attatchment_menu.add_items_to_menu(new_items)


func scroll_to_next_item(current_item:Control) -> void:
	
	# only continue if auto scrolling is enabled
	if !enable_auto_scrolling:
		return
	
	if !(current_item in menu.get_children()):
		print("ERROR: Scrolling attempted on a node that isn't a child of the menu")
	
	var distance:float = 0
	
	# find which menu was just closed
	# and move the scroll container so that it is the top item
	for i in range(0, menu.get_child_count()):
		var item := menu.get_child(i)
		
		if current_item == item:
			# scroll to the next item
			distance += item.rect_size.y
			scroll_container.scroll_vertical = distance as int
			return
		else:
			if item is Control:
				# the + 4 is the margin between items (2 top, 2 bottom)
				distance += item.rect_size.y + 4

# for the given user_names return an array of every
# device_id that is related to those names
func get_user_device_ids(user_names:Array) -> Array:
	
	var device_ids:Array = []
	
	for user_name in user_names:
		for device_id in connected_users:
			if user_name == connected_users[device_id].name:
				device_ids.append(device_id)
	
	# remove any duplicate device_ids
	# NOTE: order is not maintained when the duplicates are
	# 		remove, although for what we are doing it shouldn't
	#		matter; and .sort() can be used if need be
	var dup_array:Array = []
	for i in range(0,device_ids.size()):
		var dup:bool = false
		
		for j in range(i+1,device_ids.size()):
			print(device_ids[i] ,"\t", device_ids[j])
			if device_ids[i] == device_ids[j]:
				dup = true
				break
		if !dup:
			dup_array.append(device_ids[i])
	# replace the old array with the duplicate free array
	device_ids = dup_array
	
	return device_ids






###########
# signals #
###########


func _on_Exit_Button_pressed() -> void:
	if task.is_empty():
		close()
	else:
		popup.show()

func close() -> void:
	emit_signal("closed")
	
	field_painter.hide()
	placement_selector.hide()
	time_selector.hide()
	text_box.hide()
	self.hide()


func enable_done_button() -> void:
	done_button.disabled = false

func disable_done_button() -> void:
	done_button.disabled = true


func _on_done_button_pressed() -> void:
	var user_device_ids:Array = get_user_device_ids([task.who])
	
	# clear the 'who' since it may have changed
	# let the server fill it in
	task.who = ""
	
	task.recipient_device_ids = user_device_ids
	
	# set who is sending this task
	task.sender = Network.get_connected_users()[Network.THIS_DEVICE_ID]
	
	Network.send_created_task(task)
	
	close()


func on_option_changed(option_name:String, option):
	if option_name == "enable_auto_scrolling":
		if option is bool:
			enable_auto_scrolling = option as bool
			
		else:
			print("ERROR: enable_auto_scrolling was given a non boolean option")
