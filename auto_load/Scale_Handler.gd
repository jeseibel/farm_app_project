extends Node


# laptop (1366 x 768) dpi = 96
# iMac (5120 x 2880) dpi = 217

# this is how much things should be scaled in order to have
# fonts, ratios, etc. look the same on low DPI screens
# and high DPI screens
# 96 was the dpi of the primary development machine
var screen_scale:float = -1

func _ready():
	
	if screen_scale == -1:
		determine_screen_scale()

func get_screen_scale() -> float:
	
	if screen_scale == -1:
		determine_screen_scale()
	
	return screen_scale



func determine_screen_scale() -> void:
	
	var name:String = OS.get_name()
	
	if name == "Android" || name == "iOS":
		screen_scale = OS.get_screen_size().y / Vector2(1366, 768).y
	else:# HTML5 OSX Server Windows UWP X11
		screen_scale = OS.get_screen_dpi() / 96.0
