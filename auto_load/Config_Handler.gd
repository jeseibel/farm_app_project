extends Node

signal option_changed(option_name, option)

# on my laptop this defaults to:
# C:\Users\James_Seibel\AppData\Roaming\Godot\app_userdata\Farm_App_Project
export(String) var config_location = "user://settings.cfg"
var config:ConfigFile

var info:Config_Info = Config_Info.new()

var debug:bool = false
var debug_save_output:bool = false

var read_error:int = -1


func _ready():
	config = ConfigFile.new()
	
	read_error = config.load(config_location)
	
	if read_error == OK:
		# read the config file
		read_config()
		
		# rewrite the config file
		save_config()
		
	elif read_error == ERR_PARSE_ERROR:
		print("ERROR: the config file was unable to be read, to prevent lossing any incorrectly formatted data the program will now quit.")
		get_tree().quit(ERR_PARSE_ERROR)
		OS.alert("ERROR: the config file was unable to be read, to prevent lossing any incorrectly formatted data the program will now quit.", "Error")
	
	elif read_error == ERR_FILE_NOT_FOUND:
		print("Warning: Config file not found, creating one now...")
		read_config()
		save_config()
	
	else:
		print("ERROR: Config file reading had the error code: " , read_error)
	
	


func read_config() -> void:
	if debug:
		print("Reading config file... \n")
	
	# server #
	read_server_config()
	
	# UI #
	read_ui_config()
	
	# Task_Options
	read_task_config()
	
	# Task_Icons
	read_task_icon_config()
	
	# Vehicle_Options
	read_task_vehicle_config()
	
	if debug:
		print("\n\n")
		print("config reading complete. \n")


func read_server_config() -> void:
	var option
	
	# what is this device's username?
	option = config.get_value("Server", "username", OS.get_unique_id())
	if debug:
		print("username = ", option)
	info.username = option
	
	# get the default server ip
	option = config.get_value("Server", "server_ip", "69.178.214.91")
	if debug:
		print("server_ip = ", option)
	info.server_ip = option
	
	# should a server be created at launch?
	option = config.get_value("Server", "auto_run_server", false)
	if debug:
		print("auto_run_server = ", option)
	info.auto_run_server = option

func read_ui_config() -> void:
	var option
	
	
	# should the time be shown as relative or absolute?
	option = config.get_value("UI", "display_time_as_relative", true)
	if debug:
		print("display_time_as_relative = ", option)
	info.display_time_as_relative = option
	
	# should fields be shown from the map or satellite view?
	option = config.get_value("UI", "show_field_map_view_by_default", true)
	if debug:
		print("show_field_map_view_by_default = ", option)
	info.show_field_map_view_by_default = option
	
	# should the task creator scroll to the next item whenever an item is completed?
	option = config.get_value("UI", "enable_auto_scrolling", false)
	if debug:
		print("enable_auto_scrolling = ", option)
	info.enable_auto_scrolling = option

func read_task_config() -> void:
	var option
	
	option = config.get_value("Task_Creator", "order", ["who", "when", "vehicle", "attatchment", "goal", "where", "special_instructions"] )
	if debug:
		print("task_option order = ", option)
	info.task_option_order = option
	
	
	option = config.get_value("Task_Creator", "options", 
	{
	"who" : "Who",
	"when" : ["When SHOW_TIME_SELECTOR"],
	"vehicle" : "Vehicle",
	"attatchment" : "Attatchment",
	"goal" : ["Goal", "Drop off", "Apply to area SHOW_FIELD_PAINTER","Other\n(Enter Text) SHOW_TEXT_BOX"],
	"where" : ["Where SHOW_PLACEMENT_SELECTOR"],
	"special_instructions" : ["Instructions SHOW_TEXT_BOX"],
	}
	)
	if debug:
		print("task_options = ", option)
	info.task_options = option

func read_task_icon_config() -> void:
	var option
	
	option = config.get_value("Task_Creator", "icon_locations", 
	{
	"who" : "res://images/gui/person.png",
	"when" : "res://images/gui/clock.png",
	"vehicle" : "res://images/gui/vehicle.png",
	"attatchment" : "res://images/gui/trailer.png",
	"goal" : "res://images/gui/target.png",
	"where" : "res://images/gui/map_pin.png",
	"special_instructions" : "res://images/gui/instructions.png",
	})
	if debug:
		print("Task icons = ", option)
	info.task_icon_locations = option

func read_task_vehicle_config() -> void:
	var option
	
	option = config.get_value("Task_Creator", "vehicle_info", 
		{
		"New Red Tractor" : [["Tractor", "has PTO"] , ["Planter", "Disk"]], 
		"New Semi" : [["Semi Truck"] , ["Chem Trailer"]],
		})
	if debug:
		print("Vehicle_Options vehicle_info = ", option)
	info.task_vehicle_info = option
	
	
	option = config.get_value("Task_Creator", "vehicle_attatchments_by_tag",
		{
		"all" : [ "Current Attatchment", "Nothing" ],
		"Tractor" : ["Rock Picker", "Pulling Ropes"], 
		"has PTO" : ["Grain Belt", "Auger"],
		"Semi Truck" : ["Grain Trailer"]
		})
	if debug:
		print("Vehicle_Options vehicle_attatchments_by_tag = ", option)
	info.task_vehicle_attatchments_by_tag = option






#######################
# setters and getters #
#######################

func get_read_error() -> int:
	return read_error

func get_info() -> Config_Info:
	return info
func set_info(new_info:Config_Info) -> void:
	info = new_info
	save_config()






################
# write config #
################

func save_config() -> void:
	
	if read_error == ERR_PARSE_ERROR:
		# don't overwrite the incorrectly written data
		if debug:
			print("Saving config failed, the config file was malformed.")
		return
	
	if debug:
		print("saving config...")
	
	if debug_save_output:
		print("username ", info.username)
		print("server_ip ", info.server_ip)
		print("auto_run_server ", info.auto_run_server)
		
		print("display_time_as_relative ", info.display_time_as_relative)
		print("show_field_map_view_by_default ", info.show_field_map_view_by_default)
		print("enable_auto_scrolling ", info.enable_auto_scrolling)
		
		print("avaliable_key_words ", ["SHOW_FIELD_PAINTER", "SHOW_PLACEMENT_SELECTOR", "SHOW_TIME_SELECTOR", "RADIO_BUTTON", "SHOW_TEXT_BOX"])
		print("option_format ", ["Text_to_display_on_button", "option 1", "option 2"])
		
		print("order ", info.task_option_order)
		print("options ", info.task_options)
		
		print("Task_Creator ", "icon_locations", info.task_icon_locations)
		
		print("vehicle_info ", info.task_vehicle_info)
		print("vehicle_attatchments_by_tag ", info.task_vehicle_attatchments_by_tag)
	
	config.set_value("Server", "username", info.username)
	config.set_value("Server", "server_ip", info.server_ip)
	config.set_value("Server", "auto_run_server", info.auto_run_server)
	
	config.set_value("UI", "display_time_as_relative", info.display_time_as_relative)
	config.set_value("UI", "show_field_map_view_by_default", info.show_field_map_view_by_default)
	config.set_value("UI", "enable_auto_scrolling", info.enable_auto_scrolling)
	
	config.set_value("Task_Option_Notes", "avaliable_key_words", ["SHOW_FIELD_PAINTER", "SHOW_PLACEMENT_SELECTOR", "SHOW_TIME_SELECTOR", "RADIO_BUTTON", "SHOW_TEXT_BOX"])
	config.set_value("Task_Option_Notes", "option_format", ["Text_to_display_on_button", "option 1", "option 2"])
	
	config.set_value("Task_Creator", "order", info.task_option_order)
	config.set_value("Task_Creator", "options", info.task_options)
	
	config.set_value("Task_Creator", "icon_locations", info.task_icon_locations)
	
	config.set_value("Task_Creator", "vehicle_info", info.task_vehicle_info)
	config.set_value("Task_Creator", "vehicle_attatchments_by_tag", info.task_vehicle_attatchments_by_tag)
	
	
	var error:int = config.save(config_location)
	
	if error != OK:
		print("ERROR: Config file saving had the error code: " , error)
	else:
		if debug:
			print("Config saving complete")





#################
# chagne option #
#################

func change_config_option(option_name:String, option) -> void:
	
	match option_name:
		"username":
			info.username = option 
		"server_ip":
			info.server_ip = option
		"auto_run_server":
			info.auto_run_server = option
	
		"display_time_as_relative":
			info.display_time_as_relative = option
		"show_field_map_view_by_default":
			info.show_field_map_view_by_default = option
		"enable_auto_scrolling":
			info.enable_auto_scrolling = option
	
	
	save_config()
	
	emit_signal("option_changed", option_name, option)

