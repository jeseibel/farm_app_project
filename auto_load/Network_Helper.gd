extends Node

var image_delete_debug:bool = false

# used for storing the server's save data
var save_location:String = "user://server_backup.json"
var image_save_location:String = "user://backup_images/"
var texture_save_location:String = "user://backup_textures/"


##################
# reconstruction #
##################

func create_texture_from_dictionary(dict:Dictionary, read_from_file:bool = false) -> Texture:
	if dict == null || dict.empty():
		return null
	
	if read_from_file:
		var im:Image = Image.new()
		var error:int = im.load(dict.texture_location)
		if error != OK:
			print("Error: image: " + dict.texture_location + " error code: " + str(error))
		
		var itex:ImageTexture = ImageTexture.new()
		itex.create_from_image(im)
		
		return itex
	else:
		var im:Image = Image.new()
		im = create_image_from_dictionary(dict)
		
		var itex:ImageTexture = ImageTexture.new()
		itex.create_from_image(im)
		
		return itex

func create_image_from_dictionary(dict:Dictionary, read_from_file:bool = false) -> Image:
	if dict == null || dict.empty():
		return null
	
	if read_from_file:
		var im:Image = Image.new()
		var error:int = im.load(dict.image_location)
		if error != OK:
			print("Error: image: " + dict.texture_location + " error code: " + str(error))
		
		return im
	else:
		var im:Image = Image.new()
		im.create_from_data(dict.width, dict.height, dict.mipmaps, dict.format, dict.data)
		return im



############
# teardown #
############

func convert_image_to_dictionary(im:Image, save_to_file:bool = false) -> Dictionary:
	if im == null:
		return {}
	
	if save_to_file:
		# make sure the folder exists
		var dir:Directory = Directory.new()
		var error:int = dir.make_dir_recursive(image_save_location)
		if error != OK:
			print("ERROR creating directory: " , image_save_location)
			return {}
		
		var dict:Dictionary = {}
		
		error = im.save_png(image_save_location + str(im.data.hash()) + ".png")
		if error != OK:
			print("Error: image: ", image_save_location, str(im.data.hash()), ".png error code: ", str(error))
		
		dict.texture_location = image_save_location + str(im.data.hash()) + ".png"
		
		return dict
	else:
		
		var dict:Dictionary = im.data
		dict.format = im.get_format()
		return dict

func convert_texture_to_dictionary(tex:Texture, save_to_file:bool = false) -> Dictionary:
	if tex == null:
		return {}
	
	if save_to_file:
		# make sure the folder exists
		var dir:Directory = Directory.new()
		var error:int = dir.make_dir_recursive(texture_save_location)
		if error != OK:
			print("ERROR creating directory: ", texture_save_location)
			return {}
		
		var dict:Dictionary = {}
		var im:Image = Image.new()
		
		# convert the texture into an image
		im.create_from_data( tex.get_width(), tex.get_height(), tex.get_data().data.mipmaps, tex.get_data().get_format(), tex.get_data().data.data )
		
		error = im.save_png(texture_save_location + str(tex.get_data().data.hash()) + ".png")
		if error != OK:
			print("Error: image: ", texture_save_location, str(tex.get_data().data.hash()), ".png error code: ", str(error))
		
		dict.texture_location = texture_save_location + str(tex.get_data().data.hash()) + ".png"
		
		return dict
	else:
		
		var dict:Dictionary = tex.get_data().data
		dict.format = tex.get_data().get_format()
		return dict







#################
# Server backup #
#################


func save_server_to_file() -> void:
	
	# since we are going to write new images anyway
	# delete the old ones
	delete_old_images()
	
	
	
	var backup_file = File.new()
	var error:int = backup_file.open(save_location, File.WRITE)
	if error != OK:
		print("ERROR: unable to save server data to file, error code: " + str(error))
		return
	
	# Call the node's save function.
	var data:Dictionary = Network.get_data_as_dictionary()
	
	# Store the save dictionary as a new line in the save file.
	backup_file.store_line(to_json(data))
	
	backup_file.close()


func load_server_from_file() -> void:
	var backup_file = File.new()
	if !backup_file.file_exists(save_location):
		print("The server backup file doesn't exist, one will be created later.")
		return
	
	backup_file.open(save_location, File.READ)
	var line:String = backup_file.get_line()
	
	if line == null || line.empty():
		print("The server backup file exists, but is empty.")
		
		return
	
	
	backup_file.open(save_location, File.READ)
	# Get the saved dictionary
	var data:Dictionary = parse_json(line)
	
	# Now we set the variables.
	Network.set_data_from_dictionary(data)
	
	backup_file.close()




# this should only be called after the server has loaded every image it needs
# calling this at the start of save_server_to_file, should prevent the number
# of images growing out of control
func delete_old_images() -> void:
	
	var paths:Array = [image_save_location, texture_save_location]
	
	for path in paths:
		var error:int = 0
		var dir = Directory.new()
		
		# check if this folder exists
		if !dir.dir_exists(path):
			if image_delete_debug:
				print("the folder: " , path, " doesn't exist, skipping deleting old images.")
			continue
		
		# delete all files in the folder
		error = dir.open(path)
		if error == OK:
			dir.list_dir_begin(true)
			var file_name = dir.get_next()
			while file_name != "":
				if image_delete_debug:
					print("Deleting backup image: " + file_name)
				dir.remove(file_name)
				file_name = dir.get_next()
		else:
			print("An error occurred when trying to delete_old_images in the path: ", path )




##################
# Helper methods #
##################

# replacement for OS.get_unique_id since that doesn't work on
# some devices
func generate_random_id() -> String:
	
	var id = OS.get_unique_id()
	
	# sudo random ID generation
	if (id == ""):
		id = "ID_{" + String(RandomNumberGenerator.new().randi()) + "}"
	
	return id

