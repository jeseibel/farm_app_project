extends Node

var debug:bool = false

var font_dir:String = "res://fonts/"

func _ready():
	
	# make sure every font is scalled correctly for this display
	
	var files:Array = get_files_in_dir(font_dir)
	
	
	
	for file in files:
		# only worry about dynamic font files
		if file.count(".tres"):
			if debug:
				print("Font file found: \"", file , "\"")
			
			resize_font(file)
			
			if debug:
				print("\n")
		else:
			if debug:
				print("Non font file found: \"", file , "\"")
	


# this creates an array of each file in the given directory
# written with their whole path ie: "res://fonts/folder/font.tres"
func get_files_in_dir(path:String) -> Array:
	
	var files:Array = []
	var dir:Directory = Directory.new()
	
	if dir.open(path) != OK:
		print("ERROR: Font_Handler get_files_in_dir wasn't able to open: " + path)
		return files
	
	# warning-ignore:return_value_discarded
	dir.list_dir_begin(true)
	var file:String = dir.get_next()
	
	while file != "":
		# this is a folder, not a file
		# recursively add it's contents
		if file.count(".") == 0:
			
			# only add a "/" if the directory doesn't already have one at the end
			if path.substr(path.length() - 1) == "/":
				if debug:
					print("\nsearch folder: " + path + file)
				
				# recursively look through the next folder and add
				# each item into the files array
				for f in (get_files_in_dir(path + file)):
					files.append(f)
			else:
				if debug:
					print("\nsearch folder: " + path + "/" + file)
				
				for f in (get_files_in_dir(path + "/" + file)):
					files.append(f)
		
		# this is a file, add it and continue
		else:
			files.append(path + "/" + file)
			if debug:
				print("\tadd: " + path + "/" + file)
		
		file = dir.get_next()
	
	dir.list_dir_end()
	
	
	return files



func resize_font(font_location:String) -> void:
	
	var font:DynamicFont = load(font_location)
	var old_size:int = font.size
	
	
	font.size = (font.size * Scale_Handler.get_screen_scale()) as int
	
	if debug:
		print(old_size , " -> " , font.size)



