extends Node

class_name Network_class_name

var debug:bool = true
var backup_debug:bool = true

# this should be readonly/final/const/etc. but godot doesn't support that
# also it can't be set until _ready()
export var THIS_DEVICE_ID:String = ""


# both server and client
signal user_connected_to_server(user_dictionary)
signal user_changed_name(user_dictionary)
signal user_disconnected_from_server(user_id)

# server only
signal server_started()
# this is also emited when preparing for
# running a server
signal server_stopped()

# client only
signal connected_to_server()
signal disconnected_from_server()
signal searching_for_server()
signal server_connection_fail()

signal task_created(user_id, task)
signal task_finished(user_id, task)


# 1025 - 65535
const SERVER_PORT:int = 20301
const MAX_CONNECTIONS:int = 100
var server_ip:String = "192.168.1.18" # laptop ip

enum {SERVER_MODE, CLIENT_MODE}
var operating_mode:int = CLIENT_MODE

enum {SERVER_RUNNING, SERVER_CLOSED, CLIENT_CONNECTED_TO_A_SERVER, CLIENT_NOT_CONNECTED_TO_A_SERVER, CLIENT_SEARCHING_FOR_A_SERVER}
var network_status:int = CLIENT_NOT_CONNECTED_TO_A_SERVER


# this list contains each user's: device_id, network_id and their name
# sorted by device_id
var connected_users:Dictionary = {}

var connection_retry_timer:Timer


# this holds the data needed by the task_creator
# and owned by the server
var task_data:Dictionary = {}

var unfinished_tasks:Array = []
var completed_tasks:Array = []
var unsent_tasks:Array = []



#########
# Ready #
#########

func _ready() -> void:
	
	THIS_DEVICE_ID = Network_Helper.generate_random_id()
	
	start_server_connections()
	
	# listen to changes in the local config
	var error:int = Config_Handler.connect("option_changed", self, "on_option_changed")
	if error != OK:
		print("Config_Handler option_changed connection error code: " + str(error))
	
	# get all required information from the config file
	server_ip = Config_Handler.get_info().server_ip
	
	# set up the connection_retry_timer
	setup_timer()
	
	if Config_Handler.get_info().auto_run_server:
		start_server()
	else:
		connect_to_server()
		
		# if the client isn't connected to a server
		# auto retry until a connection is made
		if network_status == CLIENT_NOT_CONNECTED_TO_A_SERVER:
			start_server_search()

func setup_timer() -> void:
	connection_retry_timer = Timer.new()
	add_child(connection_retry_timer)
	
	connection_retry_timer.wait_time = 1.0
	
	var error:int = connection_retry_timer.connect("timeout", self, "_on_Timer_timeout")
	if error != OK:
		print("Timer timout connection error code: " + str(error))







#########
# Setup #
#########

# server

func start_server() -> void:
	operating_mode = SERVER_MODE
	
	# set up the data to be sent to connected users
	create_task_data()
	
	# if there is a save file load all avaliable data from that
	Network_Helper.load_server_from_file()
	
	# try to start the server
	var peer = NetworkedMultiplayerENet.new()
	var error:int = peer.create_server(SERVER_PORT, MAX_CONNECTIONS)
	if error != OK:
		if error == ERR_CANT_CREATE:
			print("ERROR: the server was unable to be created, check to see if there is another server already running on the same IP.")
			network_status = SERVER_CLOSED
			return
		else:
			print("ERROR: Server creation error code: " + str(error))
		return
	
	# the server was successfully started
	if debug:
		print("\n")
		print("Server started")
	
	get_tree().network_peer = peer
	
	# have the server add itself to the connected_users list
	var user:Dictionary = {}
	user.name = Config_Handler.get_info().username
	user.device_id = THIS_DEVICE_ID
	user.network_id = get_tree().network_peer.get_unique_id()
	connected_users[THIS_DEVICE_ID] = user
	
	Config_Handler.change_config_option("auto_run_server", true)
	network_status = SERVER_RUNNING
	emit_signal("server_started")

# this can also be used to prepare for starting a server
func stop_server() -> void:
	get_tree().network_peer = null
	
	Config_Handler.change_config_option("auto_run_server", false)
	
	if debug:
		if network_status == SERVER_RUNNING:
			print("\n")
			print("Server stopped")
	
	# list all users as being disconnected
	for device_id in connected_users:
		connected_users[device_id].network_id = 0
	
	network_status = SERVER_CLOSED
	# this only goes out to this program
	emit_signal("server_stopped")


# this is creates the data needed by the Task_Creator
# and should be called at the start of the server
# and every time a new user is registered
func create_task_data() -> void:
	
	# build the vehicle options
	var vehicle:Array = [Config_Handler.get_info().task_options.vehicle]
	for veh in Config_Handler.get_info().task_vehicle_info:
		vehicle.append(veh)
	
	
	# build the task_options
	var task_options:Dictionary = {
		"who" : [Config_Handler.get_info().task_options.who],
		"when" : Config_Handler.get_info().task_options.when,
		"vehicle" : vehicle,
		"attatchment" : [Config_Handler.get_info().task_options.attatchment],
		"goal" : Config_Handler.get_info().task_options.goal,
		"where" : Config_Handler.get_info().task_options.where,
		"special_instructions" : Config_Handler.get_info().task_options.special_instructions
		}
	
	# build the task_aliases
	var Menu_Type:GDScript = load("res://interface/task_creator/Menu_Type.gd")
	var task_aliases:Dictionary = {
		"who" : Menu_Type.remove_special_type_text(task_options.who[0]),
		"when" : Menu_Type.remove_special_type_text(task_options.when[0]),
		"vehicle" : Menu_Type.remove_special_type_text(task_options.vehicle[0]),
		"attatchment" : Menu_Type.remove_special_type_text(task_options.attatchment[0]),
		"goal" : Menu_Type.remove_special_type_text(task_options.goal[0]),
		"where" : Menu_Type.remove_special_type_text(task_options.where[0]),
		"special_instructions" : Menu_Type.remove_special_type_text(task_options.special_instructions[0])
		}
	
	# build the task_icon_locations
	var task_icon_locations:Dictionary = {
		"who" : Config_Handler.get_info().task_icon_locations.who,
		"when" : Config_Handler.get_info().task_icon_locations.when,
		"vehicle" : Config_Handler.get_info().task_icon_locations.vehicle,
		"attatchment" : Config_Handler.get_info().task_icon_locations.attatchment,
		"goal" : Config_Handler.get_info().task_icon_locations.goal,
		"where" : Config_Handler.get_info().task_icon_locations.where,
		"special_instructions" : Config_Handler.get_info().task_icon_locations.special_instructions
		}
	
	
	
	task_data = {
		"task_order" : Config_Handler.get_info().task_option_order,
		"task_options" : task_options,
		"task_aliases" : task_aliases,
		"task_icon_locations" : task_icon_locations,
		"vehicle_info" : Config_Handler.get_info().task_vehicle_info,
		"vehicle_attatchments_by_tag" : Config_Handler.get_info().task_vehicle_attatchments_by_tag,
		}







# client

func connect_to_server() -> void:	
	operating_mode = CLIENT_MODE
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(server_ip, SERVER_PORT)
	get_tree().network_peer = peer

func disconnect_from_server() -> void:
	get_tree().network_peer = null
	
	if debug:
		print("manually disconnected from server")
	
	# clear the network ids of all previously known users
	for device_id in connected_users:
		connected_users[device_id].network_id = 0
	
	# clear the known list of users
#	connected_users.clear()
	
	network_status = CLIENT_NOT_CONNECTED_TO_A_SERVER
	# this only goes out to this program
	emit_signal("disconnected_from_server")



# both

# this needs to be done for both the server and client
# it sets up the signals for both 
func start_server_connections() -> void:
	var error:int
	
	
	# both client and server
	error = get_tree().connect("network_peer_connected", self, "user_connected", [] , CONNECT_DEFERRED)
	if error != OK:
		print("ERROR: network_peer_connected error code: " + str(error))
	
	error = get_tree().connect("network_peer_disconnected", self, "user_disconnected", [] , CONNECT_DEFERRED)
	if error != OK:
		print("ERROR: network_peer_disconnected error code: " + str(error))
	
	
	# client only
	error = get_tree().connect("connected_to_server", self, "connected_to_a_server", [] , CONNECT_DEFERRED)
	if error != OK:
		print("ERROR: connected_to_server error code: " + str(error))
	
	error = get_tree().connect("connection_failed", self, "failed_to_connect_to_server", [] , CONNECT_DEFERRED)
	if error != OK:
		print("ERROR: connection_failed error code: " + str(error))
	
	error = get_tree().connect("server_disconnected", self, "disconnected_from_server", [] , CONNECT_DEFERRED)
	if error != OK:
		print("ERROR: server_disconnected error code: " + str(error))








###########
# Signals #
###########

# both client and server

func user_connected(network_id:int):
	# Called on both clients and the server when a peer connects. Send my info to it.
	rpc_id(network_id, "register_user", Config_Handler.get_info().username, THIS_DEVICE_ID)

remote func register_user(username:String, device_id:String):
	# Get the id of the RPC sender.
	var network_id = get_tree().get_rpc_sender_id()
	var user:Dictionary = {}
	
	# depending on if this user is new or not, change the signal emitted
	if connected_users.has(device_id):
		user = connected_users[device_id]
		
		if user.network_id != 0:
			# the user is already connected, but changed their name
			var old_name:String = user.name
			user.name = username
			connected_users[device_id] = user
			if debug:
				print(old_name , " changed their name to: " + user.name)
			
			emit_signal("user_changed_name", user)
			
			# tell the client they have been registered
			# (as long as they aren't also the server)
			if network_id != 1:
				rpc_id(network_id, "on_registered_with_server")
		else:
			# this is an old user reconnecting
			
			# update their network_id
			user.network_id = network_id
			# in case they changed their name since
			# last time they connected
			user.name = username
			connected_users[device_id] = user
			
			emit_signal("user_connected_to_server", user)
			
			if debug:
				print(user.name, " connected, device_id: ", user.device_id)
			
			
			# if this is the server, give this user
			# any tasks that they hadn't finished before
			if get_tree().network_peer.get_unique_id() == 1:
				# go through all unfinished tasks
				if debug:
					print("updating client's list of tasks")
				for task in unfinished_tasks:
					# check if this user is on the recipient list
					for id in task.recipient_device_ids:
						if id == user.device_id:
							rpc_id(network_id, "add_task_from_user", task.sender , task.create_dictionary())
			
			# tell the client they have been registered,
			# as long as they aren't also the server
			# (they can now clear their list of completed tasks)
			if network_id != 1:
				rpc_id(network_id, "on_registered_with_server")
	
	else:
		# this is a new user
		
		# create a new user object for this person
		
		user.name = username
		user.device_id = device_id
		user.network_id = network_id
		
		connected_users[device_id] = user
		
		if debug:
				print("A new user named: " , user.name, " connected, device_id: ", user.device_id)
		
		emit_signal("user_connected_to_server", user)
		
		# tell the client they have been registered
		# (as long as they aren't also the server)
		if network_id != 1:
			rpc_id(network_id, "on_registered_with_server")
	
	
	
	# since a new user joined, make sure the user list is
	# up to date and then send out the list to each person
	if get_tree().network_peer.get_unique_id() == 1:
		# only have the server send out the updated task list
		
		# update the task_data
		create_task_data()
		
		if debug:
			print("Sending updated task_data to all connected users")
		
		for device_id in connected_users:
			# only send the data to connected users
			if connected_users[device_id].network_id != 0:
				
				# don't call rpc on myself
				if device_id != THIS_DEVICE_ID:
					rpc_id(connected_users[device_id].network_id, "set_task_data", task_data)
				else:
					set_task_data(task_data)
		
		# update the backup data
		if backup_debug:
			print("Backing up server data")
		Network_Helper.save_server_to_file()


func user_disconnected(network_id:int):
	var user:Dictionary = get_user_by_network_id(network_id)
	
	
	if user.empty():
		# the user must have already been removed from the connected_users list
		return
	
	
	if debug:
		print("User disconnected: name: ", user.name, "\t network ID: ", network_id, "\t device ID: ", user.device_id)
	
	
	# the user isn't connected anymore, 
	# remove their connection info
	user.network_id = 0
	connected_users[user.device_id] = user
	
	emit_signal("user_disconnected_from_server", user)



# client only

func connected_to_a_server():
	if debug:
		print("\n")
		print("successfully connected to the server")
	network_status = CLIENT_CONNECTED_TO_A_SERVER
	connection_retry_timer.stop()
	
	Network_Helper.load_server_from_file()
	
	
	# have the client add itself to the connected_users list
	var user:Dictionary = {}
	user.name = Config_Handler.get_info().username
	user.device_id = THIS_DEVICE_ID
	user.network_id = get_tree().network_peer.get_unique_id()
	connected_users[THIS_DEVICE_ID] = user
	
	
	# send any tasks that were created 
	# while not connected
	if debug && !unsent_tasks.empty():
		print("Sending unsent tasks")
	for task in unsent_tasks:
		if debug:
			print("sending: ", task.goal.name)
		
		var send_task:Task = Task.new()
		send_task.create_from_dictionary(task, true)
		
		send_created_task(send_task)
	unsent_tasks.clear()
	
	# send any tasks that were created 
	# while not connected
	if debug && !completed_tasks.empty():
		print("Sending completed tasks")
	for task in completed_tasks:
		if debug:
			print("sending: ", task.goal.name)
		rpc_id(1, "task_done", task.create_dictionary())
	
	emit_signal("connected_to_server")

# only claer these objects after having been
# registerd by the server. This prevents the
# server from accidently given completed tasks
remote func on_registered_with_server() -> void:
	if debug:
		print("Server registration completed")
	completed_tasks.clear()

func disconnected_from_server():
	if debug:
		print("\n")
		print("disconnected from the server")
	
	network_status = CLIENT_NOT_CONNECTED_TO_A_SERVER
	emit_signal("disconnected_from_server")
	
	# clear the network ids of all previously known users
	for device_id in connected_users:
		connected_users[device_id].network_id = 0
	
	# clear the known list of users
#	connected_users.clear()
	
	# try reconnecting to the server
	start_server_search()

func failed_to_connect_to_server():
	
	if debug:
		print("server connection failed")
	network_status = CLIENT_NOT_CONNECTED_TO_A_SERVER
	
	emit_signal("server_connection_fail")









##########################
# Remote Procedure Calls #
##########################

# sender (task_creator) -> server, server -> recipients
func send_created_task(task:Task) -> void:
	
	# are we connected to a server (or the server)?
	if (network_status != CLIENT_CONNECTED_TO_A_SERVER &&
	network_status != SERVER_RUNNING):
		# we are not connected to a server
		# save the tasks to be sent once we are connected
		
		print("Client disconnected from the server, the task will be sent once the connection has been re-established")
		
		unsent_tasks.append(task)
		if backup_debug:
			print("Backing up server data")
		Network_Helper.save_server_to_file()
		return
	
	
	# am I the server or a client?
	if connected_users[THIS_DEVICE_ID].network_id == 1:
		# I am the server
		
		# send the task to each user requested
		for device_id in task.recipient_device_ids:
			
			var recipient:Dictionary = connected_users[device_id]
			
			if connected_users[device_id].network_id == 1:
				# the server already has the task,
				# don't add it again
				pass
				
			elif recipient.network_id != 0:
				# this recipient is a regular client
				print("sending task to: " , recipient.name, ",\t device_id: " , recipient.device_id, ",\t network_id: ", recipient.network_id)
				
				rpc_id(recipient.network_id, "add_task_from_user", task.sender, task.create_dictionary())
			else:
				# this recipient isn't connected right now,
				# the task will be sent later
				if debug:
					print("tried to send a task to a disconnected user, name: " , recipient.name, ",\t device_id: ", recipient.device_id )
		
	else:
		# I am a client
		
		# send the task to the server and have
		# the server distribute it
		print("sending task to The Server")
		rpc_id(1, "add_task_from_user", task.sender, task.create_dictionary())


remote func add_task_from_user(sender:Dictionary, task_dictionary:Dictionary) -> void:
	var task:Task = Task.new()
	task.create_from_dictionary(task_dictionary)
	# get who it is for
	task.who = connected_users[task.recipient_device_ids[0]].name
	
	print("task recieved from: " , sender.name, ", for: ", task.who, ", with the goal of: ", task.goal.name)
	
	# do we already have this task?
	for unfin_task in unfinished_tasks:
		if unfin_task.is_equal(task):
			print("I already have that task!")
			return
	
	unfinished_tasks.append(task)
	
	
	# am I the server or a client?
	if connected_users[THIS_DEVICE_ID].network_id == 1:
		# I am the server
		
		# send this task to each of its recipients
		
		# am I a recipient?
		var server_recip:bool = false
		for device_id in task.recipient_device_ids:
			if device_id == THIS_DEVICE_ID:
				server_recip = true
				break
		
		# I am a recipient, the task was sent here,
		# don't start an infinite loop
		if !server_recip:
			send_created_task(task)
	# else:
		# I am the client
		
		# don't do anything, just use the 'task_created'
		# signal to add the task to the Notification_Menu
	
	
	# update the backup data
	if backup_debug:
		print("Backing up server data")
	Network_Helper.save_server_to_file()
	
	emit_signal("task_created", sender, task)

# used by the server to send task data to clients
remote func set_task_data(new_task_data:Dictionary) -> void:
	
	if get_tree().get_rpc_sender_id() != 1:
		# don't let anyone but the server set the task_data
		return
	
	if debug:
		print("Recieved new task_data from the server.")
	
	task_data = new_task_data

# server -> recipients
remote func task_done(completed_task_dictionary:Dictionary) -> void:
	var completed_task:Task = Task.new()
	completed_task.create_from_dictionary(completed_task_dictionary)
	
	
	# check to see if this task exists
	var task_found:bool = false
	for task in unfinished_tasks:
		if completed_task.is_equal(task):
			task_found = true
			break
	if !task_found:
		# this task doesn't exist, a client
		# must be desynced from the server
		if debug:
			print(completed_task.who, " asked to complete a finished task")
		# let users finished completed tasks
		# I don't have a fix right now
#		return
	
	if debug:
		print(completed_task.who, " completed the task: " , completed_task.goal.name)
	
	
	
	# am I the server or the clinet?
	if get_tree().network_peer.get_unique_id() == 1:
		# I am the server
		
		# tell each recipient that this task is done
		for device_id in completed_task.recipient_device_ids:
			# is the server a recipient?
			if device_id != THIS_DEVICE_ID:
				# the server is not a recipient, continue
				if connected_users[device_id].network_id != 0:
					# the user is connected
					rpc_id(connected_users[device_id].network_id, "task_done", completed_task_dictionary)
		
	else:
		# I am a client
		
		# don't do anything special
		# just remove the task from the list
		# and remove the gui button
		# (done later and through the 'task_finished'
		# signal respecfively)
		pass
	
	
	# remove the finished task from the list of unfinished_tasks
	var dup_array:Array = []
	for task in unfinished_tasks:
		if completed_task.is_equal(task):
			pass
		else:
			dup_array.append(task)
	
	unfinished_tasks = dup_array
	
	
	# update the backup data
	if backup_debug:
		print("Backing up server data")
	Network_Helper.save_server_to_file()
	
	emit_signal("task_finished", completed_task.sender, completed_task)







#################
# Other Methods #
#################

# setters and getters

func get_network_status() -> int:
	return network_status

func get_server_ip() -> String:
	return server_ip
func set_server_ip(new_ip:String) -> void:
	server_ip = new_ip

func get_server_port() -> int:
	return SERVER_PORT

func set_operating_mode(new_operating_mode:int) -> void:
	if new_operating_mode == SERVER_MODE:
		operating_mode = SERVER_MODE
		
		# if we were searching for a server, stop, we are the server now
		stop_server_search()
		
		# if we are currently hosting a server
		# don't stop the server since we are already in server mode
		if network_status != SERVER_RUNNING:
			stop_server()
	elif new_operating_mode == CLIENT_MODE:
		operating_mode = CLIENT_MODE
		# if we are currently connected to a server
		# don't disconnect since we are already in client mode
		if network_status != CLIENT_CONNECTED_TO_A_SERVER:
			disconnect_from_server()
	else:
		print("invalid operating mode set: " + str(new_operating_mode) + ", not changing")
func get_operating_mode() -> int:
	return operating_mode

func get_connected_users() -> Dictionary:
	return connected_users.duplicate(true)

func get_unfinished_tasks() -> Array:
	return unfinished_tasks

# contains the following:
# "connected_users" -> Array of Dictionaries
# "task_order" -> Array
# "task_options" -> Dictionary
# "task_aliases" -> Dictionary
# "task_icon_locations" -> Dictionary
# "vehicle_info" -> Dictionary
# "vehicle_attatchments_by_tag" -> Dcitionary
func get_task_data() -> Dictionary:
	return task_data.duplicate(true)



# server searching

func start_server_search() -> void:
	if debug:
		print("started searching for a server")
	
	# this is so that icon changes immediately after clicking the button
	_on_Timer_timeout()
	
	connection_retry_timer.start()
	network_status = CLIENT_SEARCHING_FOR_A_SERVER

# used to try and repeatidly look for the server
func _on_Timer_timeout():
	
	emit_signal("searching_for_server")
	if (network_status == CLIENT_NOT_CONNECTED_TO_A_SERVER ||
	network_status == CLIENT_SEARCHING_FOR_A_SERVER):
		connect_to_server()
	elif network_status != CLIENT_SEARCHING_FOR_A_SERVER:
		connection_retry_timer.stop()


func stop_server_search() -> void:
	if debug:
		print("stopped searching for a server")
	
	if connection_retry_timer != null:
		connection_retry_timer.stop()
		network_status = CLIENT_NOT_CONNECTED_TO_A_SERVER
		
		emit_signal("disconnected_from_server")



# saving/loading to/from a file

# returns a dictionary holding:
# connected_users,
# unfinished_tasks,
# completed_tasks, and,
# unsent_tasks
func get_data_as_dictionary() -> Dictionary:
	var data:Dictionary = {}
	
	# create a new array of users where each network_id is 0
	# this way we don't have to worry about changing things on load
	var users:Dictionary = {}
	for device_id in connected_users:
		
		var user:Dictionary = {}
		user.device_id = device_id
		user.network_id = 0
		user.name = connected_users[device_id].name
		
		users[device_id] = user
	
	data.connected_users = users
	
	# convert the unfinished tasks into dictionaries
	var unfin_tasks:Array = []
	for task in unfinished_tasks:
		unfin_tasks.append(task.create_dictionary(false, true))
	data.unfinished_tasks = unfin_tasks
	
	# convert the completed tasks into dictionaries
	var comp_tasks:Array = []
	for task in completed_tasks:
		comp_tasks.append(task.create_dictionary(false, true))
	data.completed_tasks = comp_tasks
	
	# convert the unsent tasks into dictionaries
	var unsent:Array = []
	for task in unsent_tasks:
		unsent.append(task.create_dictionary(false, true))
	data.unsent_tasks = unsent
	
	
	return data

# overwrite the current:
# connected_users,
# unfinished_tasks,
# completed_tasks, and,
# unsent_tasks
func set_data_from_dictionary(data:Dictionary) -> void:
	
	connected_users = data.connected_users
	
	# convert the unfinished dictionaries into tasks
	for unfin_dict in data.unfinished_tasks:
		var task:Task = Task.new()
		task.create_from_dictionary(unfin_dict, true)
		
		send_created_task(task)
	
	# convert the completed dictionaries into tasks
	var comp_tasks:Array = []
	for comp_dict in data.completed_tasks:
		var task:Task = Task.new()
		task.create_from_dictionary(comp_dict, true)
		
		comp_tasks.append(task)
	completed_tasks = comp_tasks
	
	# convert the unsent dictionaries into tasks
	var unsent:Array = []
	for task in data.unsent_tasks:
		var unsent_dict:Task = Task.new()
		unsent_dict.create_from_dictionary(task, true)
		
		unsent.append(task)
	unsent_tasks = unsent



# misc

func on_option_changed(option_name:String, option) -> void:
	if option_name == "server_ip":
		server_ip = option
		if (network_status == CLIENT_CONNECTED_TO_A_SERVER || 
		network_status == CLIENT_NOT_CONNECTED_TO_A_SERVER):
			disconnect_from_server()
			start_server_search()
		elif (network_status == SERVER_CLOSED || 
		network_status == SERVER_RUNNING):
			stop_server()
			start_server()
	
	elif option_name == "username":
		
		for device_id in connected_users:
			var user:Dictionary = connected_users[device_id]
			
			if get_tree().network_peer != null && user.network_id != 0:
				# someone on the server changed their username
				if user.network_id != get_tree().network_peer.get_unique_id():
					rpc_id(user.network_id, "register_user", option, THIS_DEVICE_ID)
				else:
					# I changed my own username
					register_user(option, THIS_DEVICE_ID)


func get_user_by_network_id(network_id:int) -> Dictionary:
	
	for device_id in connected_users:
		var user:Dictionary = connected_users[device_id]
		if user.network_id == network_id:
			return user
	
	# there is no user connected with that network_id
	return {}
