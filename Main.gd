extends Node

onready var camera:Camera2D = $GCC2D
onready var gui:CanvasLayer = $GUI


var last_field:Field
var prev_m_time:int
const DOUBLE_CLICK_TIME:int = 500
# any movements less then this won't clear the last field
const NO_MOVE_THREASHOLD:float = 4.0
# how many movements below the no move threashold can happen
# before the last field is cleared
const MOVEMENTS_TO_REFRESH:int = 20
var numb_of_movments:int = 0






func _ready():
	
	# set boundries for the camera
	var x_max = $Map.width / 2
	var x_min = -x_max
	
	var y_max = $Map.height / 2
	var y_min = -y_max
	
	# set the starting location for the camera
	camera.offset = $Map/Starting_Camera_Location.position
	camera.zoom = camera.zoom / Scale_Handler.get_screen_scale()
	
	camera.x_max = x_max
	camera.y_max = y_max
	camera.x_min = x_min
	camera.y_min = y_min








func _on_Map_field_pressed(field:Field):
	
	var current_m_time:int = OS.get_ticks_msec()
	
	# only show the task creator when the field has been
	# double tapped
	if last_field == field && current_m_time - prev_m_time < DOUBLE_CLICK_TIME:
		# duplicate is required to prevent tampering
		# with the original task_options array
		gui.open_task_creator(field)
		
		last_field = null
		
	else:
		last_field = field
	
	prev_m_time = current_m_time

var old_location:Vector2

# when the camera moves, reset the last_field,
# although have a little leniency incase the user
# moved their finger slightly when tapping on the field
func _on_GCC2D_move(new_location:Vector2):
	
	if old_location != null:
		if (old_location.distance_to(new_location) > NO_MOVE_THREASHOLD || 
		numb_of_movments > MOVEMENTS_TO_REFRESH):
			# either the user moved to far or has been moving for too long,
			# forget the last field and reset the counter
			
			last_field = null
			numb_of_movments = 0
		else:
			numb_of_movments += 1
	
	old_location = new_location

# when the user zooms, reset the last_field
func _on_GCC2D_zoom(_new_zoom:Vector2):
	last_field = null



func _on_GUI_show_field_on_map(field:Field):
	camera.offset = field.rect_position + (field.rect_size / 2)



