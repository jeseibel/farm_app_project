class_name Time_Helper

extends Reference

# The only reason this is seperate from Time is because
# as of Godot version 3.2.2 stable, Godot doesn't allow
# classes to reference themselves.
# This should be fixed in Godot version 4, but until then
# this script will need to be used whenever a method
# needs to create a Time object.



# Number of days per month (except for February durring a leap year)
const month_days:Array = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

# these are used when finding the difference between two times
const MINUTES_IN_A_YEAR:int = 525960
const MINUTES_IN_A_MONTH:int = 43830
const MINUTES_IN_A_DAY:int = 1440
const MINUTES_IN_A_HOUR:int = 60





##############
# Converters #
##############

# converts this time relative to the current
# computer time to a relative time
static func convert_to_relative(time:Time) -> void:
	 
	if time.relative:
		# it is already relative,
		# no need to change it
		return
	
	
	var new_time:Time = time_until_present(time)
	
	time.relative = true
	time.relative_past = new_time.relative_past
	
	time.year = new_time.year
	time.month = new_time.month
	time.day = new_time.day
	
	time.hour = new_time.hour
	time.minute = new_time.minute


# converts this time relative to the current
# computer time to an absolute time
static func convert_to_absolute(time:Time) -> void:
	 
	if !time.relative:
		# it is already absolute,
		# no need to change it
		return
	
	time.relative = false
	
	var current_time:Dictionary = OS.get_datetime()
	
	# if the relative time is in the past
	# make everything negative to account for that
	if time.relative_past:
		time.day *= -1
		time.month *= -1
		time.year *= -1
		
		time.hour *= -1
		time.minute *= -1
	
	time.day += current_time["day"]
	time.month += current_time["month"]
	time.year += current_time["year"]
	
	time.hour += current_time["hour"]
	time.minute += current_time["minute"]
	
	correct_time(time)


# this makes sure that this time object
# is valid (May be inacurate for times over a month long), 
# if any numbers are over their maximums
# (EX: hours being over 60) then the time
# is rolled up until it is valid
# (61 minutes -> 1 hr 1 minute)
static func correct_time(time:Time) -> void:
	
	if time.relative:
		convert_to_absolute(time)
	
	
	# correct the minutes
	if time.minute >= 60:
		var add_hours:int = time.minute / 60
		time.minute = time.minute % 60
		time.hour += add_hours
	
	
	# correct the hours
	if time.hour > 24:
		var add_days:int = time.hour / 24
		time.hour = time.hour % 24
		time.day += add_days
	
	
	# correct the months
	# (this is done before days since the days requires
	# the month to be correct)
	if time.month > 12:
		var add_years:int = time.month / 12
		time.month = time.month % 12
		time.year += add_years
	
	
	# correct the days
	if time.day > month_days[time.month]:
		var add_months:int = time.month / 12
		time.month = time.month % 12
		time.year += add_months
	
	
	# correct the months again,
	# just in case the days pushed us into next month
	if time.month > 12:
		var add_years:int = time.month / 12
		time.month = time.month % 12
		time.year += add_years


static func create_time_from_dictionary(dict:Dictionary) -> Time:
	
	var time:Time = Time.new()
	
	time.relative = dict["relative"]
	time.relative_past = dict["relative_past"]
	
	time.hour = dict["hour"]
	time.minute = dict["minute"]
	
	time.day = dict["day"]
	time.month = dict["month"]
	time.year = dict["year"]
	
	return time







##################
# math functions #
##################


static func time_until_present(time:Time) -> Time:
	return time_between(time, null)

# returns the time until "new_time"
# it will be negative if "new_time" happened in the past
# if "new_time" is null then the current computer time
# will be used
static func time_between(time:Time, new_time:Time = null) -> Time:
	
	var compare_time:Dictionary
	
	# If we were given a time use that,
	# otherwise use the current time
	if new_time == null:
		compare_time = OS.get_datetime()
	else:
		compare_time = {
			"day" : new_time.day,
			"month" : new_time.month,
			"year" : new_time.year,
			
			"hour" : new_time.hour,
			"minute" : new_time.minute
		}
	
	# convert both into minutes since the year 0 AD
	var this_minutes:int = time.year * MINUTES_IN_A_YEAR
	this_minutes += time.month * MINUTES_IN_A_MONTH
	this_minutes += time.day * MINUTES_IN_A_DAY
	this_minutes += time.hour * MINUTES_IN_A_HOUR
	this_minutes += time.minute
	
	var compare_minutes:int = compare_time["year"] * MINUTES_IN_A_YEAR
	compare_minutes += compare_time["month"] * MINUTES_IN_A_MONTH
	compare_minutes += compare_time["day"] * MINUTES_IN_A_DAY
	compare_minutes += compare_time["hour"] * MINUTES_IN_A_HOUR
	compare_minutes += compare_time["minute"]
	
	
	var difference_in_mintutes:int = this_minutes - compare_minutes
	
	# convert the difference back into a Time object
	var dif:Time = Time.new()
	dif.relative = true
	
	# have the actual numbers always positive
	# but flag that this time is in the past
	if difference_in_mintutes < 0:
		dif.relative_past = true
		difference_in_mintutes = abs(difference_in_mintutes) as int
	else:
		dif.relative_past = false
	
	dif.year = difference_in_mintutes / MINUTES_IN_A_YEAR
	difference_in_mintutes %= MINUTES_IN_A_YEAR
	
	dif.month = difference_in_mintutes / MINUTES_IN_A_MONTH
	difference_in_mintutes %= MINUTES_IN_A_MONTH
	
	dif.day = difference_in_mintutes / MINUTES_IN_A_DAY
	difference_in_mintutes %= MINUTES_IN_A_DAY
	
	dif.hour = difference_in_mintutes / MINUTES_IN_A_HOUR
	difference_in_mintutes %= MINUTES_IN_A_HOUR
	
	dif.minute = difference_in_mintutes
	
	
	return dif


# determines whether or not this time object is in the past
# relative to the current computer time
static func is_in_the_past(time:Time) -> bool:
	
	if time.relative:
		return time.relative_past
	else:
		return time_between(time).relative_past


# formula via Microsoft
# https://docs.microsoft.com/en-us/office/troubleshoot/excel/determine-a-leap-year
static func is_leap_year(y:int) -> bool:
	return y % 400 == 0 || (y % 4 == 0 && y % 100 != 0)






#############
# exporting #
#############

static func create_time_string(time:Time, relative:bool) -> String:
	if relative:
		return create_relative_string(time)
	else:
		return create_absolute_string(time)

static func create_relative_string(time:Time) -> String:
	
	var return_string:String
	var is_relative:bool = time.relative
	
	if is_relative:
		return_string = time.to_string()
	else:
		convert_to_relative(time)
		return_string = time.to_string()
		convert_to_absolute(time)
	
	return return_string

static func create_absolute_string(time:Time) -> String:
	
	var return_string:String
	var is_relative:bool = time.relative
	
	if is_relative:
		convert_to_absolute(time)
		return_string = time.to_string()
		convert_to_relative(time)
	else:
		return_string = time.to_string()
	
	return return_string

static func create_dictionary(time:Time) -> Dictionary:
	var dict:Dictionary = {}
	
	dict["relative"] = time.relative
	dict["relative_past"] = time.relative_past
	
	dict["hour"] = time.hour
	dict["minute"] = time.minute
	
	dict["day"] = time.day
	dict["month"] = time.month
	dict["year"] = time.year
	
	return dict
