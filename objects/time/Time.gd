class_name Time

extends Reference

# is this time object relative to the current time?
var relative:bool = false

# this is only used for relative times
var relative_past:bool = false


var hour:int = 0
var minute:int = 0


var day:int = 0
var month:int = 0
var year:int = 0




################
# constructors #
################

func create_from_dictionary(dict:Dictionary) -> void:
	
	relative = dict["relative"]
	relative_past = dict["relative_past"]
	
	hour = dict["hour"]
	minute = dict["minute"]
	
	day = dict["day"]
	month = dict["month"]
	year = dict["year"]


func set_to_current_time() -> void:
	var current:Dictionary = OS.get_datetime()
	
	hour = current.hour
	minute = current.minute
	
	day = current.day
	month = current.month
	year = current.year







#############
# exporting #
#############

# create a string in the format:
# "In 1 year, 3 months, 2 days, 4 hours, and 32 minutes"
# or (if it is relative or not)
# "At 4:32 AM 8/6/2012"
func to_string() -> String:
	var s:String = ""
	
	if relative:
		# here are a few examples of how it should output
		# In 1 year, 3 months, 2 days, 4 hours, and 32 minutes
		# 1 year, 3 months, 2 days, 4 hours, and 32 minutes ago
		
		if !relative_past:
			s += "In "
		
		# year
		s = to_string_append_helper(s, "year", true)
		
		# month
		s = to_string_append_helper(s, "month", true)
		
		# day
		s = to_string_append_helper(s, "day", true)
		
		# hour
		s = to_string_append_helper(s, "hour", true)
		
		if year != 0 || month != 0 || day != 0 || hour != 0:
			s += "and "
		
		# minute
		s = to_string_append_helper(s, "minute", false, true, true)
		
		if relative_past:
			s += "ago"
		
	else:
		# here are a few examples of how it should output
		# At 1:30 AM, 4/7/2014
		# At 11:05 PM, 11/23/2024
		
		# determine if it is morning or afternoon
		var twelve_hour:int = hour
		var pm:bool = false
		if twelve_hour > 12:
			twelve_hour %= 12
			pm = true
		
		s += "At " + str(twelve_hour) + ":"
		
		# make sure the minutes are always 2 digits
		if minute < 10: s += "0"
		s += str(minute)
		
		if pm:
			s += " PM, "
		else:
			s += " AM, "
		
		s += str(month) + "/" + str(day) + "/" + str(year)
	
	return s

# s is the original string to append to
# part_to_add can be "year", "month", "day", "hour", or "minute"
func to_string_append_helper(s:String, part_to_add:String, end_with_comma:bool, add_even_if_zero:bool = false, make_two_digit:bool = false) -> String:
	
	var data:int
	
	match part_to_add:
		"year":
			data = year
		"month":
			data = month
		"day":
			data = day
		"hour":
			data = hour
		"minute":
			data = minute
		_:
			print("ERROR: to_string_append_helper was given: \'" + part_to_add + "\' which isn't a valid option.")
			return s
	
	# only add data if it is greater than 0
	# or if forced
	if data > 0 || add_even_if_zero:
		
		if data < 10 && make_two_digit:
			# add an extra zero to convert something like:
			# "9" -> "09"
			s += "0"
		
		s += str(data) + " " + part_to_add
		
		# make the label plural if the data was greater than 1
		if data == 0 || data > 1:
			s += "s"
		
		if end_with_comma:
			s += ", "
		else:
			s += " "
	
	return s




###################
# other functions #
###################

func is_equal(other_time:Time) -> bool:
	if relative != other_time.relative:
		return false
	if relative_past != other_time.relative_past:
		return false
	
	if hour != other_time.hour:
		return false
	if minute != other_time.minute:
		return false
	
	if day != other_time.day:
		return false
	if month != other_time.month:
		return false
	if year != other_time.year:
		return false
	
	return true
