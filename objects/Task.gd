class_name Task

extends Reference

enum {DATA_ADDING_FAILED, NEW_DATA_ADDED, DATA_OVERWRITTEN, INCORRECT_DATA_TYPE_GIVEN}

# this signal is emited when the add_data method was called
# and data slot has been filled
signal complete()

var sender:Dictionary = {}
var recipient_device_ids:Array = []

var who:String = ""
var when:Time = null
var vehicle:String = ""
var attatchment:String = ""
var goal:Goal
var field:Field = null
var parking_location:Vector2 = Vector2(-1,-1)
var parking_direction:float
var special_instructions:String = ""

var data_aliases:Dictionary = {}





################
# constructors #
################

func create_dummy_task() -> void:
	
	var new_time:Time = Time.new()
	new_time.day = 4
	new_time.month = 10
	new_time.year = 1998
	
	new_time.hour = 12
	new_time.minute = 30
	new_time.relative = false
	
	var new_goal:Goal = Goal.new()
	new_goal.name = "Goal Name"
	new_goal.location = Vector2(0.5,0.5)
	new_goal.direction = 0
	
	var tmp_tex:Texture = load("res://images/debug/painting.png")
	new_goal.image = tmp_tex.get_data()
	
	var new_field:Field = Field.new()
	new_field.map_texture = load("res://images/fields/Far_North.png")
	new_field.satellite_texture = null
	
	who = "Who"
	when = new_time
	vehicle = "vehicle Name"
	attatchment = "Attatchment Name"
	goal = new_goal
	field = new_field
	parking_location = Vector2(0.6,0.7)
	parking_direction = PI/2
	special_instructions = "Special Instructions Here"


func create_data_aliases(new_data_aliases:Dictionary) -> void:
	data_aliases["who"] = new_data_aliases["who"]
	data_aliases["when"] = new_data_aliases["when"]
	data_aliases["vehicle"] = new_data_aliases["vehicle"]
	data_aliases["attatchment"] = new_data_aliases["attatchment"]
	data_aliases["goal"] = new_data_aliases["goal"]
	data_aliases["where"] = new_data_aliases["where"]
	data_aliases["special_instructions"] = new_data_aliases["special_instructions"]



func create_from_dictionary(dict:Dictionary, save_images_and_textures_to_file:bool = false) -> void:
	
	sender = dict["sender"]
	recipient_device_ids = dict["recipient_device_ids"]
	
	who = Network.connected_users[recipient_device_ids[0]].name
	when = Time.new()
	when.create_from_dictionary(dict["when"])
	vehicle = dict["vehicle"]
	attatchment = dict["attatchment"]
	goal = Goal.new()
	goal.create_from_dictionary(dict["goal"], save_images_and_textures_to_file)
	field = Field.new()
	field.create_from_dictionary(dict["field"], save_images_and_textures_to_file)
	parking_location.x = dict["parking_location_x"]
	parking_location.y = dict["parking_location_y"]
	parking_direction = dict["parking_direction"]
	special_instructions = dict["special_instructions"]
	
	var include_data_aliases:bool = dict["include_data_aliases"] 
	if include_data_aliases:
		dict["data_aliases"] = data_aliases










####################
# set and get data #
####################

# try to add data to the task
# the data_array is the list of selected objects given by
# an expanding_menu
func add_data(data_name:String, data) -> int:
	
	var return_var:int = DATA_ADDING_FAILED
	
	# don't add any data if the alias data hasn't been created yet
	if data_aliases.empty():
		print("ERROR: task data_aliases not created yet")
		return DATA_ADDING_FAILED
	
	# TODO replace with a switch statement
	if data_name == data_aliases["who"]:
		if data is Array:
			data = data[0]
		elif !(data is String):
			return INCORRECT_DATA_TYPE_GIVEN
		
		if who.empty():
			who = data
			return_var = NEW_DATA_ADDED
		else:
			who = data
			return_var = DATA_OVERWRITTEN
	
	elif data_name == data_aliases["when"]:
		if !(data is Time):
			return_var = INCORRECT_DATA_TYPE_GIVEN
		
		if when == null:
			when = data
			return_var = NEW_DATA_ADDED
		else:
			when = data
			return_var = DATA_OVERWRITTEN
	
	elif data_name == data_aliases["vehicle"]:
		if data is Array:
			data = data[0]
		elif !(data is String):
			return INCORRECT_DATA_TYPE_GIVEN
		
		if data is Array:
			data = data[0]
		
		if vehicle.empty():
			vehicle = data
			return_var = NEW_DATA_ADDED
		else:
			vehicle = data
			return_var = DATA_OVERWRITTEN
		
	elif data_name == data_aliases["attatchment"]:
		if data is Array:
			data = data[0]
		elif !(data is String):
			return INCORRECT_DATA_TYPE_GIVEN
		
		if attatchment.empty():
			attatchment = data
			return_var = NEW_DATA_ADDED
		else:
			attatchment = data
			return_var = DATA_OVERWRITTEN
		
	elif data_name == data_aliases["goal"]:
		
		if !(data is Goal) && data is Array:
			var new_goal:Goal = Goal.new()
			new_goal.name = data[0]
			data = new_goal
		elif !(data is Goal) && !(data is Array):
			return INCORRECT_DATA_TYPE_GIVEN
		
		if goal == null:
			goal = data
			return_var = NEW_DATA_ADDED
		else:
			goal = data
			return_var = DATA_OVERWRITTEN
		
	elif data_name == data_aliases["where"]:
		if !(data is Array):
			return INCORRECT_DATA_TYPE_GIVEN
		
		if parking_location == Vector2(-1,-1):
			parking_location = data[0]
			parking_direction = data[1]
			return_var = NEW_DATA_ADDED
		else:
			parking_location = data[0]
			parking_direction = data[1]
			return_var = DATA_OVERWRITTEN
		
	elif data_name == data_aliases["special_instructions"]:
		if data is Array:
			data = data[0]
		elif !(data is String):
			return INCORRECT_DATA_TYPE_GIVEN
		
		if special_instructions.empty():
			special_instructions = data
			return_var = NEW_DATA_ADDED
		else:
			special_instructions = data
			return_var = DATA_OVERWRITTEN
		
	
	
	
	if is_complete():
		emit_signal("complete")
	
	
	# return the whether the adding was successful or not
	return return_var

# true if the data was removed
# false if the data wasn't found
func remove_data(data_name:String) -> bool:
	
	
	# TODO replace with a switch statement
	if data_name == data_aliases["who"]:
		
		who = ""
		return true
	
	elif data_name == data_aliases["when"]:
		
		when = null
		return true
	
	elif data_name == data_aliases["vehicle"]:
		
		vehicle = ""
		return true
		
	elif data_name == data_aliases["attatchment"]:
		
		attatchment = ""
		return true
		
	elif data_name == data_aliases["goal"]:
		
		goal = null
		return true
		
	elif data_name == data_aliases["where"]:
		
		parking_location = Vector2(-1,-1)
		parking_direction = 0
		return true
	elif data_name == data_aliases["special_instructions"]:
		
		special_instructions = ""
		return true
	
	# the data_name wasn't found
	# nothing was changed
	return false








#################
# other methods #
#################

func get_data_aliases() -> Dictionary:
	return data_aliases


# returns true if the data_name was found and has data
# otherwise false
func has_data(data_name:String) -> bool:
	
	if data_name == data_aliases["who"]:
		if who.empty():
			return false
		else:
			return true
	
	elif data_name == data_aliases["when"]:
		if when == null:
			return false
		else:
			return true
	
	elif data_name == data_aliases["vehicle"]:
		if vehicle.empty():
			return false
		else:
			return true
		
	elif data_name == data_aliases["attatchment"]:
		if attatchment.empty():
			return false
		else:
			return true
		
	elif data_name == data_aliases["goal"]:
		if goal == null:
			return false
		else:
			return true
		
	elif data_name == data_aliases["where"]:
		if parking_location == Vector2(-1,-1):
			return false
		else:
			return true
		
	elif data_name == data_aliases["special_instructions"]:
		if special_instructions.empty():
			return false
		else:
			return true
	
	
	# the item wasn't found
	return false


# returns whether the task is complete or not
func is_complete() -> bool:
	if who.empty():
		return false
	
	if when == null:
		return false
	
	if vehicle.empty():
		return false
	
	if attatchment.empty():
		return false
	
	if goal == null:
		return false
	
	if parking_location == Vector2(-1,-1):
		return false
	
	# special instructions aren't required
	# so don't check if they are done
	
	
	return true


# returns whether the task has any data set or not
# (outside of the automatically set data)
func is_empty() -> bool:
	if !who.empty():
		return false
	
	if when != null:
		return false
	
	if !vehicle.empty():
		return false
	
	if !attatchment.empty():
		return false
	
	if goal != null:
		return false
	
	if parking_location != Vector2(-1,-1):
		return false
	
	# special instructions aren't required
	# so don't check if they are done
	
	return true



func create_dictionary(include_data_aliases:bool = false, load_images_and_textures_from_file:bool = false) -> Dictionary:
	var dict:Dictionary = {}
	
	dict["sender"] = sender
	dict["recipient_device_ids"] = recipient_device_ids
	
	dict["who"] = who
	dict["when"] = Time_Helper.create_dictionary(when)
	dict["vehicle"] = vehicle
	dict["attatchment"] = attatchment
	dict["goal"] = goal.create_dictionary(load_images_and_textures_from_file)
	dict["field"] = field.create_dictionary(load_images_and_textures_from_file)
	dict["parking_location_x"] = parking_location.x
	dict["parking_location_y"] = parking_location.y
	dict["parking_direction"] = parking_direction
	dict["special_instructions"] = special_instructions
	
	dict["include_data_aliases"] = include_data_aliases
	if include_data_aliases:
		dict["data_aliases"] = data_aliases
	
	return dict



func is_equal(other_task:Task) -> bool:
	# ignore the recipient and sender
	
	# don't compare who, since their name
	# can chance, use the device_id instead
	if recipient_device_ids != other_task.recipient_device_ids:
		return false
	
	if !when.is_equal(other_task.when):
		return false
	if vehicle != other_task.vehicle:
		return false
	if attatchment != other_task.attatchment:
		return false
	if !goal.is_equal(other_task.goal):
		return false
	if field.name != other_task.field.name:
		return false
	if parking_location != other_task.parking_location:
		return false
	if parking_direction != other_task.parking_direction:
		return false
	if special_instructions != other_task.special_instructions:
		return false
	
	# ignore the data aliases
	
	return true
