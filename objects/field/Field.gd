class_name Field

extends Button

signal field_pressed(field_name)


var mouse_contained:bool = false

export(bool) var use_map_texture_as_icon:bool = true
export(Texture) var map_texture:Texture
export(Texture) var satellite_texture:Texture




################
# constructors #
################

# Called when the node enters the scene tree for the first time.
func _ready():
	# if this field was created in the editor
	# and the map_texture hasn't been set, set
	# the map_texture as the icon
	if map_texture == null:
		map_texture = icon
	
	# resize the field to fix the texture
	if icon != null:
		var width:int = icon.get_width()
		var height:int = icon.get_height()
		rect_size = Vector2(width,height)
	
	shortcut_in_tooltip = false
	
	mouse_filter = MOUSE_FILTER_PASS
	
	# remove the selection box visual
	enabled_focus_mode = FOCUS_NONE

func create_from_dictionary(dict:Dictionary, load_texture_from_file:bool = false) -> void:
	rect_position.x = dict["position_x"]
	rect_position.y = dict["position_y"]
	rect_size.x = dict["rect_size_x"]
	rect_size.y = dict["rect_size_y"]
	
	use_map_texture_as_icon = dict["use_map_texture_as_icon"]
	
	if dict["map_texture"] != null:
		map_texture = Network_Helper.create_texture_from_dictionary(dict["map_texture"], load_texture_from_file)
	if dict["satellite_texture"] != null:
		satellite_texture = Network_Helper.create_texture_from_dictionary(dict["satellite_texture"], load_texture_from_file)
	
	_ready()




#########
# input #
#########

# pass on the input if the event is contained by the field
# (this allows the user to drag, zoom, etc. on top of a field)
func _input(event:InputEvent):
	
	if mouse_contained:
		# pretend this node is part of the background
		# send its input directly to the touch manager
		TouchManager._unhandled_input(event)
	
	elif event is InputEventMouse:
		if is_location_contained(event.position):
			TouchManager._unhandled_input(event)

func is_location_contained(pos:Vector2) -> bool:
	if (pos.x > rect_position.x &&
		pos.y > rect_position.y &&
		pos.x < rect_size.x &&
		pos.y < rect_size.x
	):
		return true
	else:
		return false



func _on_Field_mouse_entered():
	mouse_contained = true

func _on_Field_mouse_exited():
	mouse_contained = false

func _on_Field_pressed():
	emit_signal("field_pressed",self)







##################
# seters/getters #
##################

func get_satellite_texture() -> Texture:
	return satellite_texture
func get_map_texture() -> Texture:
	return map_texture






#############
# exporters #
#############

func create_dictionary(save_texture_to_file:bool = false) -> Dictionary:
	var dict:Dictionary = {}
	
	dict["position_x"] = rect_position.x
	dict["position_y"] = rect_position.y
	dict["rect_size_x"] = rect_size.x
	dict["rect_size_y"] = rect_size.y
	
	dict["use_map_texture_as_icon"] = use_map_texture_as_icon
	
	# convert textures to images and then 
	# get the raw data from those images
	dict["map_texture"] = Network_Helper.convert_texture_to_dictionary(map_texture, save_texture_to_file)
	
	dict["satellite_texture"] = Network_Helper.convert_texture_to_dictionary(satellite_texture, save_texture_to_file)
	
	return dict
