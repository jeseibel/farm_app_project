class_name Goal

extends Reference

var name:String
var location:Vector2
var direction:float
var image:Image




func create_from_dictionary(dict:Dictionary, load_image_from_file:bool = false) -> void:
	
	name = dict["name"]
	
	location.x = dict["location_x"]
	location.y = dict["location_y"]
	direction = dict["direction"]
	
	if dict.has("image") && !dict["image"].empty():
		if load_image_from_file:
			image = load(dict["image"]["texture_location"])
		else:
			image = Network_Helper.create_image_from_dictionary(dict["image"])


func create_dictionary(save_image_to_file:bool = false) -> Dictionary:
	var dict:Dictionary = {}
	
	dict["name"] = name
	
	dict["location_x"] = location.x
	dict["location_y"] = location.y
	dict["direction"] = direction
	dict["image"] = Network_Helper.convert_image_to_dictionary(image, save_image_to_file)
	
	return dict



func is_equal(other_goal:Goal) -> bool:
	
	if name != other_goal.name:
		return false
	if location != other_goal.location:
		return false
	if direction != other_goal.direction:
		return false
	
	# this will be expensive
	if image == null || other_goal.image == null:
		if (image == null && other_goal.image != null or
		image != null && other_goal.image == null):
			return false
	elif image.get_data() != other_goal.image.get_data():
		return false
	
	
	return true
