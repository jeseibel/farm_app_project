class_name Config_Info

extends Reference

# Server
var username:String = ""
var server_ip:String = ""
var auto_run_server:bool = false

var known_user_list:Array = []

# UI
var display_time_as_relative:bool = false
var show_field_map_view_by_default:bool = false
var enable_auto_scrolling:bool = false



# task #

# Task Options
var task_option_order:Array
var task_options:Dictionary

var task_icon_locations:Dictionary

var task_vehicle_info:Dictionary
var task_vehicle_attatchments_by_tag:Dictionary



